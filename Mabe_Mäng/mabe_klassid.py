from mabe_funkt import *

#t66t- kas nupp töötab;eba_t66t_v2rv-nupu värv, kui see ei tööta; valitud_raam-raami laius, kui see on valitud, Pilt-kas see on pilt
class Nupp():
    def __init__(self,nimi,tekst,y,x,minlaius=20,mink6rgus=20,fondisuurus=fondisuurus,v2rv=teksti_v2rv,taust=tausta_v2rv,
                 raam=False, t66t=True,eba_t66t_v2rv=hall,raami_laius=1,valitud_raam=3,valitud=False,joondus="vasak",maks_laius=1000, Pilt=None):
        #Saadab selle, kui klõpsatakse
        self.nimi=nimi
        self.y=y
        self.x=x
        self.tekst=tekst
        self.min_laius=minlaius
        self.min_k6rgus=mink6rgus
        self.maks_laius=maks_laius
        #Et raami ja värvi saaks algset kaotamata muuta
        self.raam_s2te=raam
        self.taust_s2te=taust
        self.valitud=valitud 
        self.raam=raam
        self.taust=taust
        self.fondisuurus=fondisuurus
        self.v2rv=v2rv
        self.raami_laius=raami_laius
        self.valitud_raam=valitud_raam
        self.joondus=joondus
        self.Pilt=Pilt
        #Kas nupp töötab
        self.t66t=t66t
        self.eba_t66t_v2rv=eba_t66t_v2rv
        self.h6ljund=False
        #Kas sunniviisiliselt laius s2titud
        self.sundlaius=False
        if Pilt:
            #self.pilt=pygame.Surface(self.pildisuurus)
            #Muudab tausta, et ei oleks mustaga sekkuv
            #self.pilt.fill(praak_v2rv)
            #self.fail=pygame.image.load(Pilt)
            #pygame.blit(self.pilt,self.fail,(0,0))
            
            self.pind=self.Pilt.copy()
            self.laius=min(max(self.pind.get_width(),self.min_laius),self.maks_laius)
            self.k6rgus=max(self.pind.get_height(),self.min_k6rgus)
        else:
            #Et oleks kohe võimalik vaadata, kas klõpsati
            self.font=pygame.font.SysFont(Kirjastiil, self.fondisuurus)
            self.kiri=self.font.render(self.tekst,True,self.v2rv)
            self.laius=min(max(self.kiri.get_width()+10,self.min_laius),self.maks_laius)
            self.k6rgus=max(self.kiri.get_height()+10,self.min_k6rgus)
        if self.t66t:
            self.kast=pygame.Rect(self.y, self.x, self.laius-1, self.k6rgus-1)
        else:
            self.kast=pygame.Rect(0,0,0,0) 

    #Kas hiir nupul peal
    def hiir(self):
        mousePos = pygame.mouse.get_pos()
        self.taust=self.taust_s2te
        self.raam=self.raam_s2te
        if self.valitud:
            self.raam=True
        self.h6ljund=False
        if self.kast.collidepoint(mousePos):
            if self.t66t:
                self.raam=True
                self.h6ljund=True
        #Kui katkine ja tekstiv2rv valge, siis yldse ei joonista
        if not (not self.t66t and self.eba_t66t_v2rv==tausta_v2rv):
            self.joonis()
            
    #Joonistab nupu
    def joonis(self):
        if self.Pilt:
            self.pind=self.Pilt.copy()
        else:
            self.kiri=self.font.render(self.tekst,True,self.v2rv if self.t66t else self.eba_t66t_v2rv)
            if not self.sundlaius:
                self.laius=max(self.kiri.get_width()+10,self.min_laius)
                if not self.h6ljund:
                    self.laius=min(self.laius,self.maks_laius)
            self.pind=pygame.Surface((self.laius,self.k6rgus))
            self.pind.fill(self.taust)

            self.taane=(max(self.kiri.get_width()+10,self.min_laius)-self.kiri.get_width())/2 if self.joondus=="kesk" else 5
            self.pind.blit(self.kiri,(self.taane,(self.k6rgus-self.kiri.get_height())/2))
        
        if (self.raam_s2te and self.h6ljund and self.t66t) or self.valitud:
            self.raami_laius=self.valitud_raam
        else:
            self.raami_laius=1
            
        if self.raam:
            pygame.draw.rect(self.pind,self.v2rv if self.t66t else self.eba_t66t_v2rv,(0,0,self.laius,self.k6rgus),self.raami_laius)
      
        aken.blit(self.pind,(self.y,self.x,self.laius,self.k6rgus))
    #Vaatab, kas midagi juhtunud       
    def klikk(self):
        for event in pygame.event.get():                
            if event.type==pygame.MOUSEBUTTONDOWN:
                return(self.nimi)
    #Kui tahad midagi muuta
    def muudatus(self,y=None,x=None,tekst=None,v2rv=None,taust=None,fondisuurus=None,raam=None,t66t=None, laius=None, k6rgus=None):
        if y:
            self.y=y
        if x:
            self.x=x
        if taust:
            self.taust_s2te=taust
        if fondisuurus: 
            self.fondisuurus=fondisuurus
            self.font=pygame.font.SysFont(Kirjastiil, self.fondisuurus)
        if raam!=None:
            self.raam_s2te=raam
        if t66t==True:
            self.t66t=True  
        elif t66t!=None:
            self.t66t=False
            self.kast=pygame.Rect(0,0,0,0)
        #Kui laiust resettida vaja
        if laius=="vaiki":
            self.laius=max(self.kiri.get_width()+10,self.min_laius) if not self.Pilt else self.Pilt.get_width()
            sundlaius=False
        elif laius!=None:
            self.laius=laius
            self.sundlaius=True
        if k6rgus!=None:
            self.k6rgus=k6rgus
        if y or x or t66t!=None or laius!=None or k6rgus!=None:
            self.kast=pygame.Rect(self.y, self.x, self.laius-1, self.k6rgus-1)  
        
#Jah/ei väärtusega nupp
class Lyliti(Nupp):
    def __init__(self,nimi,x,y,valitud=False,pikkus=20,v2rv=teksti_v2rv,raami_laius=1,t66t=True,eba_t66t_v2rv=hall,tekst="",fondisuurus=fondisuurus, valitud_raam=3,Pilt=None):
        super().__init__(nimi,tekst,x,y,minlaius=pikkus,mink6rgus=pikkus,v2rv=v2rv,fondisuurus=1 if tekst=="" else fondisuurus,
                         raam=True,raami_laius=raami_laius,valitud_raam=valitud_raam,eba_t66t_v2rv=eba_t66t_v2rv, Pilt=Pilt,t66t=t66t)
        #Tõeväärtus
        self.v22rtus=valitud
    def joonis(self):
        super().joonis()
        if self.v22rtus:
            pygame.draw.rect(self.pind,self.v2rv,(0,0,self.laius,self.k6rgus),self.valitud_raam if (self.tekst!="" or self.Pilt!=None) else 0)
        aken.blit(self.pind,(self.y,self.x))
    #Kui klõpsati, muudab väärtust
    def klikk(self):
        if self.h6ljund:
            self.v22rtus=(not self.v22rtus)
            self.valitud=self.v22rtus
        


#Tekstikast, kuhu saab kirjutada. Valikulised väärtused tyhi, maks, lubat(ud klahvid, numbri_klahvid on sätetes list)
class Tekstikast(Nupp):
    def __init__(self,nimi,tekst,y,x,tyhi="",maks="",makspikkus=100,lubat=[],minlaius=20,mink6rgus=20,fondisuurus=fondisuurus,
                 v2rv=teksti_v2rv,taust=tausta_v2rv,raam=True,t66t=True,eba_t66t_v2rv=hall,raami_laius=1,maks_laius=1000):
        #Superklass
        super().__init__(nimi,tekst,y,x,minlaius=minlaius,mink6rgus=mink6rgus,fondisuurus=fondisuurus,v2rv=v2rv,taust=taust,
                        raam=raam,t66t=t66t,eba_t66t_v2rv=eba_t66t_v2rv,raami_laius=2, maks_laius=maks_laius)
        #Kas kast on valitud ja sinna saab kirjutada
        self.valitud=False
        #Mis väärtus on kastis, kui täiesti kustutatud(nt. numbritel 0)
        self.tyhi=tyhi
        #Numbritel maksimum, kui pole enam valitud ja tekst sellest suurem, siis muutub selleks
        self.maks=maks
        if self.maks:
            self.makspikkus=len(self.maks)
        else:
            self.makspikkus=makspikkus
        #Lubatud sümbolid, kui None, siis kõik
        self.lubat=lubat
        #Kas just valiti kast
        self.just=False
        
    def joonis(self):
        if self.valitud:
            self.taust=valitud_v2rv
        else:
            self.taust=self.taust_s2te
        self.sundlaius=False
        super().joonis()
        
    def sisend(self,klahv):
        if self.valitud:
            a=self.tekst
            #for event in pygame.event.get():               
            #    if event.type == KEYDOWN:
            if klahv.key == K_BACKSPACE:
                #Kustutab
                if len(self.tekst)>0:                            
                    self.tekst=self.tekst[:-1]                        
                if len(self.tekst)==0:
                    self.tekst=self.tyhi
            #Kui on lubatud klahvide hulgas ja mitte liiga suur/pikk
            elif (self.lubat==[] or klahv.unicode in self.lubat) and (len(self.tekst)<self.makspikkus+1 or self.maks==""):
                
                if self.tekst==self.tyhi or self.just:
                    self.tekst=klahv.unicode

                else:
                    #Kui pole liiga pikk
                    if len(self.tekst+klahv.unicode)<len(self.maks) or self.maks=="" or (len(self.tekst+klahv.unicode)==len(self.maks) and self.tekst+klahv.unicode<self.maks):
                        self.tekst += klahv.unicode
                    else:                        
                        self.tekst= self.maks
            self.just=False if a!=self.tekst else self.just

#Valimine mitme nupu vahelt(nupud:mis nupu nimi on, tekstid, mis nupul kirjas on,Lylitism-list, kui true, siis lyliti on true, kui none, siis nupud pole lylitis)
class Valik():
    def __init__(self,nimi,nupud,tekstid,y,x,minlaius=20,mink6rgus=20,vahe=20,fondisuurus=fondisuurus,
                v2rv=teksti_v2rv,taust=tausta_v2rv,raam=False, t66t=True,eba_t66t_v2rv=hall,raami_laius=1,
                 suund="hor",valitud=None,maks_laius=1000, Pilt=None,lylitism=None,valitud_raam=3):
        self.nimi=nimi
        self.vahe=vahe
        self.x=x
        self.y=y
        if Pilt==None:
            Pilt=[None for i in range(0,len(nupud))]
        #K'sileb seda, kas nupud on llitid
        self.lylitism=lylitism
        if not lylitism:
            self.lylitism=list(False for number in range(len(nupud)))
            
        #Teeb nupud
        if lylitism:
            self.nupulist=[Lyliti(nupud[0],y,x,valitud=self.lylitism[0],fondisuurus=fondisuurus,v2rv=v2rv,
                            t66t=t66t,eba_t66t_v2rv=eba_t66t_v2rv,raami_laius=raami_laius,Pilt=Pilt[0],
                              tekst=tekstid[0],valitud_raam=valitud_raam)]
            for nupp in range(1,len(nupud)):
                self.nupulist.append(Lyliti(nupud[nupp],(self.nupulist[nupp-1].y+self.nupulist[nupp-1].laius+vahe) if suund=="hor" else y,
                    (self.nupulist[nupp-1].x+self.nupulist[nupp-1].k6rgus+vahe) if suund=="vert" else x,
                    valitud=self.lylitism[nupp], fondisuurus=fondisuurus,v2rv=v2rv,t66t=t66t,eba_t66t_v2rv=eba_t66t_v2rv,
                    raami_laius=raami_laius,Pilt=Pilt[nupp], tekst=tekstid[nupp],valitud_raam=valitud_raam))  
        else:
            self.nupulist=[Nupp(nupud[0],tekstid[0],y,x,valitud=False,minlaius=minlaius,mink6rgus=mink6rgus, fondisuurus=fondisuurus,v2rv=v2rv,
                                taust=taust,raam=raam, t66t=t66t,eba_t66t_v2rv=eba_t66t_v2rv,raami_laius=raami_laius,maks_laius=maks_laius,Pilt=Pilt[0],valitud_raam=valitud_raam)]
            for nupp in range(1,len(nupud)):
                self.nupulist.append(Nupp(nupud[nupp],tekstid[nupp],(self.nupulist[nupp-1].y+self.nupulist[nupp-1].laius+vahe) if suund=="hor" else y,
                    (self.nupulist[nupp-1].x+self.nupulist[nupp-1].k6rgus+vahe) if suund=="vert" else x,minlaius=minlaius,mink6rgus=mink6rgus, fondisuurus=fondisuurus,
                    v2rv=v2rv,taust=taust,raam=raam, t66t=t66t,eba_t66t_v2rv=eba_t66t_v2rv,raami_laius=raami_laius,maks_laius=maks_laius,Pilt=Pilt[nupp],valitud_raam=valitud_raam))  
        if valitud==None:
            self.valitud=None
        else:
            self.valitud=valitud
            self.sea(self.valitud)
        self.h6ljund=False
    #Kas hiir selle peal 
    def hiir(self):
        for nupp in self.nupulist:
            nupp.hiir()
        #Kas m6ni nupp valitud
        h6list=[nupp.h6ljund for nupp in self.nupulist]
        self.h6ljund=True in h6list
    #Kui klikitakse selle peal
    def klikk(self):
        flag=None
        a=0
        for nupp in self.nupulist:
            if nupp.h6ljund:
                flag=a
                nupp.valitud=True
                self.v22rtus=nupp.nimi
                self.valitud=a
                if type(nupp)==Lyliti:
                    nupp.klikk()
            else:
                nupp.valitud=False
            a+=1
        return(flag)
    #Nagu 'set'
    def sea(self,valik):
        t=0
        for nupp in self.nupulist:
            if nupp.nimi==valik or valik==t:
                nupp.valitud=True
            else:
                nupp.valitud=False
            t+=1
        
        self.v22rtus=valik
    #Lülitab mõned nupud välja (katki on list mitte töötavate nuppude järjenumbritest, t66t list nuppudest, mis nüüd töötavad)
    def ei(self,katki,t66t):
        for kat in katki:
            self.nupulist[kat].muudatus(t66t=False)
        for t66 in t66t:      
            self.nupulist[t66].muudatus(t66t=True)
    #Millised lylitid True. Tohib kasutada ainult siis, kui nupud on lylitid
    def Lylist(self):
        t6elist=[]
        for nupp in self.nupulist:
            t6elist.append(nupp.v22rtus)
        return(t6elist)
    #Leiab nupu indeksi
    def leia(self,nimi):
        t=0
        for nupp in self.nupulist:
            if nimi==nupp.nimi:
                return t
            t+=1
    def muudatus(self,y=None,x=None,tekst=None,v2rv=None,taust=None,fondisuurus=None,raam=None,t66t=None, laius=None, k6rgus=None):
        a=0
        if x!=None:
            self.x=x
        for nupp in self.nupulist:
            nupp.muudatus(y=(y if a==0 else self.nupulist[a-1].y+self.nupulist[a-1].laius+self.vahe),x=x,tekst=tekst,v2rv=v2rv,taust=taust,fondisuurus=fondisuurus,raam=raam,t66t=t66t,laius=laius,k6rgus=k6rgus)

#Pikk Loeng(Muutujad sama mis Valik, laius on kasti laius, k6rgus on nähtavate lahtrite arv vertikaalse loendi puhul, samm on arv, mille võrra edasi-tagasi nupud valikut liigutavad) 
class Pikk_Valik(Valik):
    def __init__(self,nimi,nupud,tekstid,y,x,laius,fondisuurus=fondisuurus,
            v2rv=teksti_v2rv,taust=tausta_v2rv,suund="hor",valitud=None,k6rgus=0,samm=1,
            Pilt=None,lylitism=None,valitud_raam=3):
        self.j2rg=0
        self.samm=samm
        self.laius=laius
        self.k6rgus=k6rgus
        self.eel=Nupp("Eelmine","<" if suund=="hor" else "▲",y,x,raam=True,minlaius=0 if suund=="hor" else laius, joondus="kesk")
        self.j2r=Nupp("Järgmine",">" if suund=="hor" else "▼",y,x,raam=True,minlaius=0 if suund=="hor" else laius, joondus="kesk")
        self.suund=suund
        super().__init__(nimi,nupud,tekstid,y+self.eel.laius if suund=="hor" else y,x if suund=="hor" else x+self.eel.k6rgus,fondisuurus=fondisuurus,
                v2rv=v2rv,taust=taust,raam=True, minlaius=1 if suund=="hor" else laius, t66t=True,eba_t66t_v2rv=tausta_v2rv,
                suund=suund,valitud=valitud,vahe=-1,maks_laius=1000 if suund=="hor" else laius,Pilt=Pilt, lylitism=lylitism,valitud_raam=valitud_raam)
        if Pilt and self.suund=="hor":
            self.eel.muudatus(k6rgus=self.nupulist[0].k6rgus)
            self.j2r.muudatus(k6rgus=self.nupulist[0].k6rgus)
        if self.suund=="hor":
            self.j2r.muudatus(y=self.j2r.y+laius-self.j2r.laius)
        else:
            self.j2r.muudatus(x=x+self.eel.k6rgus+(self.nupulist[0].k6rgus-1)*(self.k6rgus))
            
        #Et kohe paar asja teeks
        if self.suund=="hor":
            #Milline nupp on lyhendatud
            self.lyh=0
            katkilist=[]
            a=0
            for nupp in self.nupulist:
                if nupp.y>=self.j2r.y:
                    katkilist.append(a)   
                #Kui osaliselt kattunud nupp
                elif nupp.y+nupp.laius>=self.j2r.y:
                    nupp.muudatus(laius=self.j2r.y-nupp.y+1)
                    self.lyh=a
                a+=1
        else:
            katkilist=list(range(0,self.j2rg))+list(range(self.j2rg+self.k6rgus,len(self.nupulist)))
        super().ei(katkilist,[])
        if self.j2rg<=0:
            self.eel.muudatus(t66t=False)
            self.j2rg=0
        if self.j2rg>=len(self.nupulist):
            self.j2r.muudatus(t66t=False)
            self.j2rg=0
    #Kas hiir mõne nupu peal
    def hiir(self):
        #Piirjooned
        if self.suund=="hor":
            pygame.draw.line(aken,teksti_v2rv,(self.eel.y+self.eel.laius,self.eel.x),(self.j2r.y,self.eel.x),1)
            pygame.draw.line(aken,teksti_v2rv,(self.eel.y+self.eel.laius,self.eel.x+self.eel.k6rgus-1),(self.j2r.y,self.eel.x+self.eel.k6rgus-1),1)
        else:
            pygame.draw.line(aken,teksti_v2rv,(self.eel.y,self.eel.x+self.eel.k6rgus),(self.j2r.y,self.j2r.x),1)
            pygame.draw.line(aken,teksti_v2rv,(self.eel.y+self.eel.laius-1,self.eel.x+self.eel.k6rgus),(self.j2r.y+self.eel.laius-1,self.j2r.x),1)
        #Vaatab iga all-nupu l2bi
        super().hiir()
        self.eel.hiir()
        self.j2r.hiir()
        self.h6ljund=True in (self.h6ljund,self.eel.h6ljund,self.j2r.h6ljund)
        
    #Kui vajutatakse edasi-tagasi nuppe
    def liikumine(self,samm):
        if self.suund=="hor":
            #Lyhendatud pole enam lyhendatud
            self.nupulist[self.lyh].muudatus(laius="vaiki")
            #Mis nuppude pikkuse v6rra nihutada
            vasak_list=list(range(max(0,self.j2rg+samm),self.j2rg) if samm<=0 else range(self.j2rg,min(len(self.nupulist),self.j2rg+samm)))
            #Et j2rg ei läheks üle listi pikkuse või alla nulli
            self.j2rg=max(min(self.j2rg+samm,len(self.nupulist)-1),0)
            #Kui palju v6rra nupud nihkusid
            nihe=(sum(self.nupulist[nupp].laius for nupp in vasak_list)-len(vasak_list))*(1 if samm<=0 else -1)
            #Nihutab nuppe
            for nupp in self.nupulist:
                nupp.muudatus(y=nupp.y+nihe)
            
            #Mis nupud ekraanil
            #Juhuks, kui lyhendatud nuppu pole
            flag=False
            a=0
            for nupp in self.nupulist:
                if nupp.y>=self.j2r.y:
                    pass
                #Kui osaliselt kattunud nupp
                elif nupp.y+nupp.laius>=self.j2r.y:
                    nupp.muudatus(laius=self.j2r.y-nupp.y+2)        
                    self.lyh=a
                    flag=True
                a+=1
            if flag:
                listik=list(range(self.j2rg,self.lyh+1))
            else:
                listik=list(range(self.j2rg,len(self.nupulist)))
        #VERTIKAALNE
        else:
            #Mis nupud nyyd t66tavad
            vasak_list=list(range(max(0,self.j2rg+samm),self.j2rg) if samm<=0 else range(self.j2rg,min(len(self.nupulist),self.j2rg+samm)))
            self.j2rg=max(min(self.j2rg+samm,len(self.nupulist)-1),0)
            listik=list(range(self.j2rg,min(self.j2rg+self.k6rgus,len(self.nupulist))))
            #Nihutab nuppe
            for nupp in self.nupulist:
                nupp.muudatus(x=nupp.x+(nupp.k6rgus-len(vasak_list))*(1 if samm<=0 else -1))
                
        eil=list(range(0,self.j2rg))+list(range(listik[-1]+1,len(self.nupulist)))
        
        super().ei(eil,listik)
        #if samm<=0:
            #super().ei(parem_list,vasak_list)
        #else:
            #super().ei(vasak_list,parem_list)
        #Et edasi-tagasi nupud t66taks
        if self.j2r.t66t==False:
            self.j2r.muudatus(t66t=True)
        if self.eel.t66t==False:
                self.eel.muudatus(t66t=True)
        if self.j2rg<=0:
            self.eel.muudatus(t66t=False)
            self.j2rg=0
        if self.j2rg>=len(self.nupulist)-1:
            self.j2r.muudatus(t66t=False)
            self.j2rg=len(self.nupulist)-1
    #Kui klikitakse selle peal
    def klikk(self):
        #Liigutab ette, taha, kui sinna klikiti
        if self.eel.h6ljund and self.eel.t66t:
            self.liikumine(-self.samm)
        elif self.j2r.h6ljund and self.j2r.t66t:
            self.liikumine(self.samm)
        #Muudab, mis nupud töötavad
        else:
            a=super().klikk()
            if self.suund=="hor":
                if self.nupulist[self.lyh].h6ljund:
                    self.liikumine(self.lyh-self.j2rg)
            return(a)
        
                
        
    

#Mabendi üldklass. Valge pool 1, musta -1
class Mabend():
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2ht,t2htis=False):
        self.j2rg=j2rg
        self.pool=pool
        self.rida=rida
        self.veerg=veerg
        
        self.t2htis=t2htis
    
        self.vahe_vasak=vahe_vasak
        self.vahe_peal=vahe_peal
        self.ruudupikkus=ruudupikkus
        
        self.y=self.vahe_vasak+(self.veerg-1)*self.ruudupikkus
        self.x=self.vahe_peal+(self.rida-1)*self.ruudupikkus
        
        self.k2igud=[]
        self.v6tud=[]
        self.tuli=[]
        
        self.t2ht=t2ht
        if self.pool==-1:
            self.t2ht=self.t2ht.lower()
        
        self.valitud=False
        #Kas mabend v6etud
        self.koolnud=False
        
        self.kast=pygame.Rect(self.y, self.x, self.ruudupikkus, self.ruudupikkus)
    
    #Teeb mabendi katki
    def surnd(self):
        self.koolnud=True
        self.kast=pygame.Rect(0,0,0,0)
    
    #Kas vajutatakse mabendile
    def klikk(self,laud):
        if self.kast.collidepoint(pygame.mouse.get_pos()):
            self.valitud=True
    #Kui mabend on valitud
    def uuri(self,laud):
        if self.koolnud:
            self.k2igud=[]
            self.v6tud=[]
            self.tuli=[]
        else:
            self.liik_uuendus(laud)
    
    #Tyhi liik_uuendus, mida allklassid saavad muuta
    def liik_uuendus(self,laud):
        pass
        
#Kabe mabend        
class K_mabend(Mabend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus, t=False,t2htis=False):
        tykk="T" if t else "N"
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,tykk,t2htis=t2htis)
    #Kuhu saab mabend käia
    def liik_uuendus(self,laud):
        self.v6tud=[]
        self.k2igud=[]
        self.tuli=[]
        if laud[self.rida+self.pool][self.veerg+1]=='_':
            self.k2igud.append((self.rida+self.pool,self.veerg+1))
        if laud[self.rida+self.pool][self.veerg-1]=='_':
            self.k2igud.append((self.rida+self.pool,self.veerg-1))
        for r in range (-1,2,2):
            for v in range (-1,2,2):
                #Kui väli pole sein ega tühi ning on vastaspoolel
                if laud[self.rida+r][self.veerg+v] not in('S','_') and kumb_pool(laud[self.rida+r][self.veerg+v])==self.pool*-1:
                    #Kui väli eelmise taga on tühi
                    if laud[self.rida+r*2][self.veerg+v*2]=='_':
                        self.v6tud.append((self.rida+r*2,self.veerg+v*2))
                        self.tuli.append((self.rida+r,self.veerg+v))
                        
                        #Vaatab rekursiivselt, kas veel saab v6tta
                        l_vana=[self.k2igud,self.v6tud,self.tuli,self.rida,self.veerg,laud]
                        testlaud=self.k2ik(laud,(self.rida+r*2,self.veerg+v*2),t6sine=False)[0]
                        self.liik_uuendus(testlaud)
                        self.k2igud=l_vana[0]
                        self.v6tud=l_vana[1]
                        self.tuli=list(set(l_vana[2]+self.tuli))
                        self.rida=l_vana[3]
                        self.veerg=l_vana[4]
                        laud=l_vana[5]
                        
        self.k2igud+=self.v6tud               
        #Vaatab rekursiivselt k6iki v6ttude jadasid
        #for v6tt in self.v6tud:
               
    #Mabendi liikumine, v6tmine(liik on liikumine ehk ruut, kuhu minnakse(rida,veerg))
    def k2ik(self,laud,liik,t6sine=True):
        laud=[rida.copy() for rida in laud]
        v6tt=None
        #Kui peab v6tma
        
        
        if liik in self.v6tud:
            #Mis suunas liigub
            suund=(int((liik[0]-self.rida)/abs(liik[0]-self.rida)),int((liik[1]-self.veerg)/abs(liik[1]-self.veerg)))
            #Vaatab k6ik selles suunas nupud l6pppunktini l2bi, kas see on see, mida v6etakse(Tamkaga töötav)
            for i in range(abs(self.rida-liik[0])):
                if laud[self.rida+i*suund[0]][self.veerg+i*suund[1]] not in ("S","_"):
                    v6tt=(self.rida+i*suund[0],self.veerg+i*suund[1])
                    laud[self.rida+i*suund[0]][self.veerg+i*suund[1]]="_"
            #Liigutab valitud mabendit
            laud[self.rida][self.veerg]="_"
            self.rida=liik[0]
            self.veerg=liik[1]
            laud[liik[0]][liik[1]]=self.t2ht
        #Kui lihtsalt k2ik
        elif (liik[0],liik[1]) in self.k2igud:
            laud[self.rida][self.veerg]="_"
            self.rida=liik[0]
            self.veerg=liik[1]
            laud[liik[0]][liik[1]]=self.t2ht
         
        if t6sine:
            #Uuendab hitboxi
            self.y=self.vahe_vasak+(self.veerg-1)*self.ruudupikkus
            self.x=self.vahe_vasak+(self.rida-1)*self.ruudupikkus
            self.kast=pygame.Rect(self.y, self.x, self.ruudupikkus, self.ruudupikkus)  
        
        return(laud,v6tt)
    
    
#Tamka
class Tamka(K_mabend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=False):
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t=True,t2htis=t2htis)
    #Kus saab nupp võtta 
    def liik_uuendus(self,laud):

        self.v6tud=[]
        self.k2igud=[]
        self.tuli=[]
        
        #Suund
        for r in range (-1,2,2):
            for v in range (-1,2,2):
                v6tt=False
                #Kaugus algpunktist
                for k in range(1,len(laud)):
                    if laud[self.rida+r*k][self.veerg+v*k]=='_':
                        if v6tt:
                            self.v6tud.append((self.rida+r*k,self.veerg+v*k))
                            #Vaatab rekursiivselt, kas veel saab v6tta
                            l_vana=[self.k2igud,self.v6tud,self.tuli,self.rida,self.veerg,laud]
                            testlaud=self.k2ik(laud,(self.rida+r*k,self.veerg+v*k),t6sine=False)[0]
                            self.liik_uuendus(testlaud)
                            self.k2igud=l_vana[0]
                            self.v6tud=l_vana[1]
                            self.tuli=list(set(l_vana[2]+self.tuli))
                            self.rida=l_vana[3]
                            self.veerg=l_vana[4]
                            laud=l_vana[5]
                        else:
                            self.k2igud.append((self.rida+r*k,self.veerg+v*k))
                    elif laud[self.rida+r*k][self.veerg+v*k]=='S' or kumb_pool(laud[self.rida+r*k][self.veerg+v*k])==self.pool:
                        break
                    #Kui väli pole sein ega tühi ning on vastaspoolel
                    elif kumb_pool(laud[self.rida+r*k][self.veerg+v*k])==self.pool*-1:
                        if v6tt:
                            break
                        #Kui väli eelmise taga on tühi
                        if laud[self.rida+r*(k+1)][self.veerg+v*(k+1)]=='_':
                            v6tt=True
                            self.tuli.append((self.rida+r*k,self.veerg+v*k))
                        else:
                            break
        self.k2igud+=self.v6tud
            
#Üldine malend 
class Malend(Mabend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2ht,t2htis=False):
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2ht,t2htis=t2htis)
    #K2igu mabendi-poolne osa
    def k2ik(self,laud,liik,t6sine=True):
        laud=[rida.copy() for rida in laud]
        v6tt=None
        #Kui v6tt, ytleb seda
        if laud[liik[0]][liik[1]]!="_":
            v6tt=liik[0],liik[1]
            
        
        #Liigutab nupu
        laud[self.rida][self.veerg]="_"
        self.rida=liik[0]
        self.veerg=liik[1]
        laud[liik[0]][liik[1]]=self.t2ht
        
        
        if t6sine:
            #Uuendab hitboxi
            self.y=self.vahe_vasak+(self.veerg-1)*self.ruudupikkus
            self.x=self.vahe_peal+(self.rida-1)*self.ruudupikkus
            self.kast=pygame.Rect(self.y, self.x, self.ruudupikkus, self.ruudupikkus)  
        
        return[laud,v6tt]
    
#Ettur      
class Ettur(Malend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=False):
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,"E",t2htis=t2htis)
        self.liikund=False
    #Kuhu saab käia
    def liik_uuendus(self,laud):
        self.k2igud=[]
        self.v6tud=[]
        self.tuli=[]
        #Lihtk2ik
        if laud[self.rida+self.pool][self.veerg]=="_":
            self.k2igud.append((self.rida+self.pool,self.veerg))
            if not self.liikund and laud[self.rida+(self.pool*2)][self.veerg]=="_":
                self.k2igud.append((self.rida+(self.pool*2),self.veerg))
        #Diagonaalid
        if laud[self.rida+self.pool][self.veerg+1] not in("S","_") and kumb_pool(laud[self.rida+self.pool][self.veerg+1])!=self.pool:
            self.v6tud.append((self.rida+self.pool,self.veerg+1))
        if laud[self.rida+self.pool][self.veerg-1] not in("S","_") and kumb_pool(laud[self.rida+self.pool][self.veerg-1])!=self.pool:
            self.v6tud.append((self.rida+self.pool,self.veerg-1))
        self.k2igud+=self.v6tud
        #Lisab tule alla etturi v6tud
        self.tuli+=[(self.rida+self.pool,self.veerg+1),(self.rida+self.pool,self.veerg-1)]
        
    def k2ik(self,laud,liik,t6sine=True):
        if t6sine:
            self.liikund=True
        return(super().k2ik(laud,liik,t6sine=t6sine))
        
#Oda
class Oda(Malend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=False):
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,"O",t2htis=t2htis)
    #Kuhu saab k2ia
    def liik_uuendus(self,laud):
        self.k2igud=[]
        self.v6tud=[]
        self.tuli=[]
        #Suund
        for r in range (-1,2,2):
            for v in range (-1,2,2):
                #Kaugus algpunktist
                for k in range(1,len(laud)): 
                    if laud[self.rida+r*k][self.veerg+v*k]=='_':
                        self.k2igud.append((self.rida+r*k,self.veerg+v*k))
                    elif laud[self.rida+r*k][self.veerg+v*k]=='S' or kumb_pool(laud[self.rida+r*k][self.veerg+v*k])==self.pool:
                        break
                    #Kui väli pole sein ega tühi ning on vastaspoolel
                    elif kumb_pool(laud[self.rida+r*k][self.veerg+v*k])==self.pool*-1:
                        self.v6tud.append((self.rida+r*k,self.veerg+v*k))
                        self.tuli.append((self.rida+r*k,self.veerg+v*k))
                        break
        self.k2igud+=self.v6tud
        self.tuli+=self.k2igud

#Vanker
class Vanker(Malend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=False):
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,"V",t2htis=t2htis)
        self.liikund=False
        
    #Kuhu saab k2ia
    def liik_uuendus(self,laud):
        self.k2igud=[]
        self.v6tud=[]
        self.tuli=[]
        #Suund
        for r in range (-1,2):
            for v in range (-1,2):
                if abs(r)==abs(v):
                    continue
                #Kaugus algpunktist
                for k in range(1,len(laud)):
                    if laud[self.rida+r*k][self.veerg+v*k]=='_':
                        self.k2igud.append((self.rida+r*k,self.veerg+v*k))
                    elif laud[self.rida+r*k][self.veerg+v*k]=='S' or kumb_pool(laud[self.rida+r*k][self.veerg+v*k])==self.pool:
                        break
                    #Kui väli pole sein ega tühi ning on vastaspoolel
                    elif kumb_pool(laud[self.rida+r*k][self.veerg+v*k])==self.pool*-1:
                        self.v6tud.append((self.rida+r*k,self.veerg+v*k))
                        self.tuli.append((self.rida+r*k,self.veerg+v*k))
                        break
        
            
        self.k2igud+=self.v6tud
        self.tuli+=self.k2igud
        
    #Et saaks vangerdada
    def k2ik(self,laud,liik,t6sine=True):
        if t6sine:
            self.liikund=True
        return(super().k2ik(laud,liik,t6sine=t6sine))

#Ratsu
class Ratsu(Malend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=False):
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,"R",t2htis=t2htis)
    #Kuhu saab k2ia
    def liik_uuendus(self,laud):
        self.k2igud=[]
        self.v6tud=[]
        self.tuli=[]
        
        #Peab ette-vaatlik, et mitte out-of-bounds
        #Kas saab kahe v6rra vasakule, paremale
        kylg_liik=[]
        if self.veerg>2:
            kylg_liik.append(-2)
        if self.veerg<len(laud[1])-2:
            kylg_liik.append(2)
        #Kas saab kahe v6rra yles, alla
        vert_liik=[]
        if self.rida>2:
            vert_liik.append(-2)
        if self.rida<len(laud)-2:
            vert_liik.append(2)

        
        #Kyljed
        for kylg in kylg_liik:
            for suund in range(-1,2,2):
                self.tuli.append((self.rida+suund,self.veerg+kylg))
                if laud[self.rida+suund][self.veerg+kylg]=="_":
                    self.k2igud.append((self.rida+suund,self.veerg+kylg))
                elif kumb_pool(laud[self.rida+suund][self.veerg+kylg])==-self.pool:
                    self.v6tud.append((self.rida+suund,self.veerg+kylg))
                
        #Yles-alla
        for suund in vert_liik:
            for kylg in range(-1,2,2):
                self.tuli.append((self.rida+kylg,self.veerg+suund))
                if laud[self.rida+suund][self.veerg+kylg]=="_":
                    self.k2igud.append((self.rida+suund,self.veerg+kylg))
                elif kumb_pool(laud[self.rida+suund][self.veerg+kylg])==-self.pool:
                    self.v6tud.append((self.rida+suund,self.veerg+kylg))

        
        
        self.k2igud+=self.v6tud

#Lipp
class Lipp(Malend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=False):
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,"L",t2htis=t2htis)
    #Kuhu saab k2ia
    def liik_uuendus(self,laud):
        self.k2igud=[]
        self.v6tud=[]
        self.tuli=[]
        
        #Suund
        for r in range (-1,2,1):
            for v in range (-1,2,1):
                #Kaugus algpunktist
                for k in range(1,len(laud)): 
                    if laud[self.rida+r*k][self.veerg+v*k]=='_':
                        self.k2igud.append((self.rida+r*k,self.veerg+v*k))
                    elif laud[self.rida+r*k][self.veerg+v*k]=='S' or kumb_pool(laud[self.rida+r*k][self.veerg+v*k])==self.pool:
                        break
                    #Kui väli pole sein ega tühi ning on vastaspoolel
                    elif kumb_pool(laud[self.rida+r*k][self.veerg+v*k])==self.pool*-1:
                        self.v6tud.append((self.rida+r*k,self.veerg+v*k))
                        self.tuli.append((self.rida+r*k,self.veerg+v*k))
                        break
        
            
        self.k2igud+=self.v6tud
        self.tuli+=self.k2igud
        
#Kuningas
class Kuningas(Malend):
    def __init__(self,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=False):
        super().__init__(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,"K",t2htis=t2htis)
        self.liikund=False
    #Kuhu saab k2ia
    def liik_uuendus(self,laud):
        self.k2igud=[]
        self.v6tud=[]
        self.tuli=[]
        
        for r in range (-1,2):
            for v in range (-1,2):
                if laud[self.rida+r][self.veerg+v]=='_':
                    self.k2igud.append((self.rida+r,self.veerg+v))
                #Kui väli pole sein ega tühi ning on vastaspoolel
                elif kumb_pool(laud[self.rida+r][self.veerg+v])==self.pool*-1:
                    self.v6tud.append((self.rida+r,self.veerg+v))
                    continue
                                 
        self.k2igud+=self.v6tud
        self.tuli+=self.k2igud
        
    #Et saaks vangerdada
    def k2ik(self,laud,liik,t6sine=True):
        if t6sine:
            self.liikund=True
        return(super().k2ik(laud,liik,t6sine=t6sine))
