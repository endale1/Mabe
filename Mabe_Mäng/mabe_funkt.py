from s2tted import *
import os
import copy

pygame.init()
I=pygame.display.Info()
#Akna suurus(768 sest google ytles, et see tavaline v2iksema ekraani suurus)
aknake=min(768, I.current_h) if (I.current_h<1000 or v2ike) else 1000
Suur=aknake>800
aken= pygame.display.set_mode((laua_laius+2*äär,aknake))
clock = pygame.time.Clock()
pygame.key.set_repeat(300,20)

#V6tab s6ne ja muudab muutujateks
def s6nest_muutujad(s6nad,vahe=",",s6n_vahe=(":"),s6nastik=False):
    s6n=[asi.split(s6n_vahe) if s6nastik else asi for asi in s6nad.strip().split(vahe)]
    muutujad={} if s6nastik else []
    #Parandab katkised listid
    Flag=False
    while not Flag:
        katki=None
        Flag=True
        for asi in range(len(s6n)):
            if len(s6n[asi])>1:
                #Leiab katkise listi alguse
                if s6n[asi][1][0]=="[" and s6n[asi][1][-1]!=']':
                    katki=asi
                    Flag=False
            #Leiab katkise listi l6pu
            if katki!=None:
                if s6n[asi][0][-1]=="]":
                    s6n[katki]=[s6n[katki][0],[s6n[katki][-1][1:]]+[sise[0] for sise in s6n[katki+1:asi]]+[s6n[asi][0][:-1]]]
                    s6n=s6n[:katki+1]+s6n[asi+1:]
                    katki=None
                    break
    for s6ne in s6n:
        if s6ne==['']:
            continue
        temps=(s6ne[-1] if s6nastik else s6ne)
        if type(temps)==list:
            a=temps
        #Kui Bool
        elif temps in ("True","False"):
            a=True if temps=="True" else False
        #Kui list, siis rekursiivne
        elif temps[0]=="[":
            a=s6nest_muutujad(temps[1:-1],s6nastik=False)
        #Kui list, siis rekursiivne
        elif temps[0]=="{":
            a=s6nest_muutujad(temps[1:-1],s6nastik=True)
        #Kui arv
        elif temps.isdigit() or temps[1:].isdigit():
            a=int(temps)
        #Kui lihtsalt s6ne
        else:
            a=s6ne[-1]
        #Lisab s6nastikku v6i listi
        if s6nastik:
            muutujad[s6ne[0]]=a
        else:
            muutujad.append(a)
    return(muutujad)

#Võtab laua vastava nimega failist
def laud_failist(nimi):
    lauad=open(os.path.join("Kaardid",nimi+'.txt'),"r", encoding="UTF-8")
    laud=[]
    flag=False
    #Kui millegip2rast tyhi fail
    pikkus=8
    laius=8
    #Vaikimisi omadused
    omadused=vaike_omadused
    for rida in lauad:
        #Otsib õige laua
        if not flag:
            pikkus=int(rida.strip().split(';')[1])
            laius=int(rida.strip().split(';')[2])
            omadused_uus=s6nest_muutujad(rida.strip().split(';')[3],vahe="|",s6n_vahe="=",s6nastik=True)
            flag=True
        else:
            if rida.strip().split(';')[1]=='lopp':
                for omadus in list(omadused_uus.keys()):
                    omadused[omadus]=omadused_uus[omadus]
                return(laud,omadused)
            laud.append(rida.strip().split(';'))
    lauad.close()
    if laud==[]:
        laud=lauatabel(8,8)
    return(laud,omadused)
    

#Kirjutab laua faili
def laud_faili(nimi,laud,omadused_uus):
    lauad=open(os.path.join("Kaardid",nimi+'.txt'),"w", encoding="UTF-8")
    omadused=vaike_omadused
    for omadus in list(omadused_uus.keys()):
        omadused[omadus]=omadused_uus[omadus]
    pikkus=len(laud)-2
    laius=len(laud[0])-2
    omadused_s6ne=("|".join([str(v6ti)+"="+str(omadused[v6ti]).replace(" ", "") for v6ti in omadused.keys()])).replace("'","")
    #Kirjutab laua esimesse ritta laua nime, pikkuse, laiuse, ja muud omadused
    lauad.write(nimi+";"+str(pikkus)+";"+str(laius)+";"+omadused_s6ne+';\n')
    
    for rida in laud:
        lauad.write(';'.join(rida)+'\n')
    lauad.write(';lopp\n')
    lauad.close()

#def logid_faili()
    
#Võtab sõnastiku uuendatud sätetest ja nende uutest väärtustest ning uuendab sätete faili
def uuenda_s2tted(uued):
    failinimi="s2tted.py"
    s2tted=[]
    #Viib vanad sätted listi
    vana=open(failinimi,"r", encoding="UTF-8")
    for rida in vana:
        s2tted.append(rida)
    vana.close()
    #Asendab muudetud sätted uute väärtustega
    uus=list(uued.keys())
    for s2te in uus:
        for i in range(len(s2tted)):
            rida=s2tted[i]
            #Kui õige säte
            if s2te==rida.split("=")[0]:
                s2tted[i]=(s2te+"="+str(uued[s2te])+'\n')

    
    #Uuendab sätete faili
    fail = open(failinimi,"w", encoding="UTF-8")
    for rida in s2tted:
        fail.write(rida)
        
    
    fail.close()
    
    
    
        
#Liigub redaktoris antud hulga käikude võrra(logi käigud formaadis [y,x, Algne mabend, lõppmabend]). Saadab tagasi laua seisu.
#Käik-mis käik parasjagu on. Suund- Mitme võrra edasi/tagasi
#Koordinaat pole tähega salvestatud, sest läheb sassi, kui tähestiku muutujat muudetakse
def R_logi(laud,logi, k2ik,suund):
    #Kas positiivne või negatiivne
    m2rk=int(abs(suund)/suund)
    #Et plussiga töötaks
    if m2rk==1:
        k2ik-=1
    for i in range(m2rk,suund+m2rk,m2rk):
        rida=int(logi[k2ik+i][1])
        veerg=int(logi[k2ik+i][0])
        if suund<0:
            laud[rida][veerg]=logi[k2ik+i][2]
        if suund>0:
            laud[rida][veerg]=logi[k2ik+i][3]
    
    return laud

#Tuvastab, kumb pool nupul on(1 on valge,-1 on must)
def kumb_pool(mabend):
    if mabend in ("S","_"):
        return(0)
    #Valge
    if mabend.upper()==mabend:
        return(1)
    #Must
    else:
        return(-1)

#Prindib välja laua seisu
def printlaud(laud):
    for rida in laud:
        print(" ".join(rida))
    

#Tagastab sobiva tabeli
def lauatabel(laius,k6rgus):
    laud=[]
    laud.append(("S "*(laius+1)+"S").split(" "))
    for i in range(k6rgus):
        laud.append(("S "+"_ "*(laius)+"S").split(" "))
    laud.append(("S "*(laius+1)+"S").split(" "))
    return(laud)