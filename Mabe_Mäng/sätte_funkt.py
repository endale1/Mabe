from menüü_funkt import *
from graafika_funkt import (ruudujoonis,v2rvi_vahetus)


def v2rvi_menüü():
    font=pygame.font.SysFont(Kirjastiil, fondisuurus//2)
    v2ike_font=pygame.font.SysFont(Kirjastiil, fondisuurus//2)
    
    #Menüü nupud
    Tagasi=Nupp("Tagasi","Tagasi",420,aknake-60)
    Lähtesta=Nupp("Lähtesta","Lähtesta",540,aknake-60)
    Rakenda=Nupp("Rakenda","Rakenda",700,aknake-60)
    
    #Näitenupud
    t66tav=Nupp("Näitenupp","Vajutatav nupp",500,aknake-200)
    mitt=Nupp("Näitenupp2", "Mitte vajutatav nupp",500,aknake-150,t66t=False)
    nupud=[Rakenda, Tagasi,t66tav,mitt,Lähtesta]
    
    samm=(40 if aknake<800 else 60)
    
    #Lähtestamiseks
    algv2rvid={"v2rv_1":v2rv_1,"v2rv_2":v2rv_2,"mabendi_v2rv_1":v2rv_1,"mabendi_v2rv_2":v2rv_2,"teksti_v2rv":teksti_v2rv,"tausta_v2rv":tausta_v2rv,"seina_v2rv":seina_v2rv,"punane":punane,"hall":hall,"valitud_v2rv":valitud_v2rv}
    #Seab üles eri värvide jaoks asjad
    v2rvid={"v2rv_1":v2rv_1,"v2rv_2":v2rv_2,"mabendi_v2rv_1":v2rv_1,"mabendi_v2rv_2":v2rv_2,"teksti_v2rv":teksti_v2rv,"tausta_v2rv":tausta_v2rv,"seina_v2rv":seina_v2rv,"punane":punane,"hall":hall,"valitud_v2rv":valitud_v2rv}    
    v2rvi_v22rtused=list(v2rvid.values())
    #List tekstikastidest iga v2rvi rgb-ga
    v2rv_list=[[Tekstikast((i,j),str(v2rvi_v22rtused[i][j]),460+100*j,78+samm*i,minlaius=40,mink6rgus=20,maks="255",fondisuurus=fondisuurus//2, lubat=numbri_klahvid,tyhi="0") for j in range(3)] for i in range(len(v2rvi_v22rtused))]

    #Tekstid värvide kirjelduseks
    kirjeldus=["Mustade ruutude värv","Valgete ruutude värv","Mustade mabendite värv","Valgete mabendite värv","Teksti ja kastide raamide värv", "Tausta värv", "Seina mabendi värv", "Redaktoris mabendi kustutamise värv", "Mitte töötavate nuppude värv","Valitud mabendi ja selle võimalike käikude" ]
    
    #Seab üles tekstikasti valiku
    Valitud_r=None
    Valitud_v=None
    
    for v2 in v2rv_list[9]:
        v2.maks="205"
    
    Muut=False
    
    #Seab valmis etturi pildi värvide demonstratsiooni jaoks
    ettur=pygame.Surface((80,80))
    #Muudab tausta, et ei oleks mustaga sekkuv
    ettur.fill(praak_v2rv)
    ettur.blit(pygame.image.load(os.path.join("Pildid",mabendi_rada,"E.png")),(0,0))
    
    while True:
        for event in pygame.event.get():
            #Suleb m'ngu
            if event.type == QUIT:
                if Muut:
                    lahku=valik("Salvesta sätted enne sulgemist?",["Jah","Ei","Tagasi"])
                    if lahku=="Jah":
                        s2tt=list(v2rvid.keys())
                        for v2rv in range(len(v2rvid)):
                            v2rvid[s2tt[v2rv]]=(tuple(int(kast.tekst) for kast in v2rv))
                        uuenda_s2tted(v2rvid)
                    elif lahku=="Ei":
                        return 3  
                else:
                    return 3
            elif event.type == KEYDOWN:
                #Lahkub mängust, kui ESC 
                if event.key == K_ESCAPE:
                    if Muut:
                        lahku=valik("Salvesta sätted enne lahkumist?",["Jah","Ei","Tagasi"])
                        if lahku=="Jah":
                            s2tt=list(v2rvid.keys())
                            for v2rv in range(len(v2rvid)):                     
                                v2rvid[s2tt[v2rv]]=(tuple(int(kast.tekst) for kast in v2rv))
                            uuenda_s2tted(v2rvid)
                            teade("Muutuste nägemiseks sule ja ava mäng",2000)
                        elif lahku=="Ei":
                            return 2  
                    return(2)
                #Kirjutab ainult siis, kui mingi tekstikast on valitud
                if Valitud_r!=None and Valitud_r!=None:                             
                    if event.key in (K_RETURN,K_TAB):
                        v2rv_list[Valitud_r][Valitud_v].valitud=False
                        #Kui liiga suur, muudab 255-ks
                        if v2rv_list[Valitud_r][Valitud_v].tekst>"255":
                            v2rv_list[Valitud_r][Valitud_v].tekst="255"
                            Muut=True
                        #Kui shifti vajutatakse 
                        if event.mod & pygame.KMOD_SHIFT:
                            #Võtab eelmise kasti lahti
                            if Valitud_v==0:
                                if Valitud_r==0:
                                    Valitud_r=None
                                    Valitud_v=None
                                else:
                                    Valitud_v=2
                                    Valitud_r-=1
                            else:
                                Valitud_v-=1
                        else:
                            #Võtab järgmise kasti lahti
                            if Valitud_v==2:
                                if Valitud_r==9:
                                    Valitud_r=None
                                    Valitud_v=None
                                else:
                                    Valitud_v=0
                                    Valitud_r+=1
                            else:
                                Valitud_v+=1
                        v2rv_list[Valitud_r][Valitud_v].valitud=True
                        v2rv_list[Valitud_r][Valitud_v].just=True
                                
                    #Sisend
                    else:
                        a=v2rv_list[Valitud_r][Valitud_v].tekst
                        v2rv_list[Valitud_r][Valitud_v].sisend(event)
                        if a!=v2rv_list[Valitud_r][Valitud_v].tekst:
                            Muut=True
                    
                                
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                pos=pygame.mouse.get_pos()
                #y=pos[0]
                #x=pos[1]
                #Vaatab, kas menüü nuppudele on vajutatud
                
                #Tagasi
                if Tagasi.h6ljund:
                    if Muut:
                        lahku=valik("Salvesta sätted enne lahkumist?",["Jah","Ei","Tagasi"])
                        if lahku=="Jah":
                            s2tt=list(v2rvid.keys())
                            for v2rv in range(len(v2rvid)):
                                v2rvid[s2tt[v2rv]]=(tuple(int(kast.tekst) for kast in v2rv_list[v2rv]))
                            uuenda_s2tted(v2rvid)
                            teade("Muutuste nägemiseks sule ja ava mäng",2000)
                            return 2
                        elif lahku=="Ei":
                            return 2
                    else:
                        return 2
                #Rakenda
                if Rakenda.h6ljund:
                    s2tt=list(v2rvid.keys())
                    for v2rv in range(len(v2rvid)):
                        v2rvid[s2tt[v2rv]]=(tuple(int(kast.tekst) for kast in v2rv_list[v2rv]))
                    uuenda_s2tted(v2rvid)
                #L2htesta
                if Lähtesta.h6ljund:
                    #if valik("Kas sa oled kindel?",["Jah","Ei"])=="Jah":
                    v2rvid=algv2rvid
                    uuenda_s2tted(v2rvid)
                    v2rv_list=[[Tekstikast((i,j),str(v2rvi_v22rtused[i][j]),460+100*j,78+samm*i,minlaius=40,mink6rgus=20,maks="255",fondisuurus=fondisuurus//2, lubat=numbri_klahvid) for j in range(3)] for i in range(len(v2rvi_v22rtused))]
                    Muut=False
                        
                #Vaatab, kas on vajutatud tekstikastidele
                for v2rv in v2rv_list:
                    for kast in v2rv:
                        if kast.h6ljund:
                            if Valitud_r!=None and Valitud_v!=None:
                                v2rv_list[Valitud_r][Valitud_v].valitud=False
                            Valitud_r=kast.nimi[0]
                            Valitud_v=kast.nimi[1]
                            kast.valitud=True
                            kast.just=True
                        
                    
                    
                    
        aken.fill(tausta_v2rv)
        
        
        #Kirjutab roheline, punane ja sinine
        aken.blit(font.render("Punane",True,teksti_v2rv),(450,40))
        aken.blit(font.render("Roheline",True,teksti_v2rv),(545,40))
        aken.blit(font.render("Sinine",True,teksti_v2rv),(655,40))
        for r in range(len(v2rvid)):
            #Kirjutab värvide kirjeldused 
            aken.blit(font.render(kirjeldus[r],True,teksti_v2rv),(20,80+samm*r))
            #Vahele jooned
            pygame.draw.line(aken,teksti_v2rv,(10,90+samm*(r-0.5)),(laua_laius+äär*2-20,90+samm*(r-0.5)),1)
            for v2rv in v2rv_list:
                for kast in v2rv:
                    kast.hiir()
            #Teeb värviga näidiskasti
            pygame.draw.rect(aken,(tuple(int(kast.tekst) for kast in v2rv_list[r])),(800,80+samm*r,20,20))
            pygame.draw.rect(aken,teksti_v2rv,(800,80+samm*r,20,20),1)
            
        if aknake>800:
            #Kui mabendi värvid on need, mis asju katki teevad
            if tuple(int(kast.tekst) for kast in v2rv_list[2]) in (praak_v2rv,vahe_v2rv):
                aken.blit(v2ike_font.render("Praegune värv võib tekitada mabendite värvimisega probleeme",True,teksti_v2rv),(460,220))
            if tuple(int(kast.tekst) for kast in v2rv_list[3]) in (praak_v2rv,vahe_v2rv):
                aken.blit(v2ike_font.render("Praegune värv võib tekitada mabendite värvimisega probleeme",True,teksti_v2rv),(460,280))
        #Kirjutab viimase värvi kirjelduse teise rea
        aken.blit(font.render("ning valitud tekstikasti värv. (Maks 205)",True,teksti_v2rv),(20,460 if aknake<800 else 650))
        #Näitenupud õiget värvi
        t66tav.v2rv=tuple(int(kast.tekst) for kast in v2rv_list[4])
        mitt.eba_t66t_v2rv=tuple(int(kast.tekst) for kast in v2rv_list[8])
        
        #Näitenuppudele raam ümber
        pygame.draw.rect(aken,tuple(int(kast.tekst) for kast in v2rv_list[5]),(498,aknake-202,300,100))
        pygame.draw.rect(aken,tuple(int(kast.tekst) for kast in v2rv_list[4]),(498,aknake-202,300,100),1)
        
        #Rakenda ja Tagasi nupud
        for nupp in nupud:
            nupp.hiir()
                                 
        Rakenda.t66t=Muut                        

        v2rv_t6lk=[tuple(int(kast.tekst) for kast in v2rv_list[0]),tuple(int(kast.tekst) for kast in v2rv_list[1])]

        #Näitemängulaud
        #Valged ja mustad ruudud
        for r in range (2):
            for v in range(3):
                ruudujoonis(r,v,v2rv_t6lk[r%2!=v%2],80,20,aknake-220)
        #Valitud ala         
        ruudujoonis(0,3,tuple(int(kast.tekst)+50 for kast in v2rv_list[9]),80,20,aknake-220)
        ruudujoonis(1,3,tuple(int(kast.tekst) for kast in v2rv_list[9]),80,20,aknake-220)
        #Sein
        ruudujoonis(0,1,tuple(int(kast.tekst) for kast in v2rv_list[6]),80,20,aknake-220)
        
        #Etturid õiget värvi ja pildile
        must=v2rvi_vahetus(ettur,(0,0,0),vahe_v2rv)
        must=v2rvi_vahetus(must,(255,255,255),tuple(int(kast.tekst) for kast in v2rv_list[2]))
        must=v2rvi_vahetus(must,vahe_v2rv,tuple(int(kast.tekst) for kast in v2rv_list[3]))
        must.set_colorkey(praak_v2rv)
        aken.blit(must,(260,aknake-140))
        
        #Teeb valge versiooni mabendist ja lisab sõnastikku
        valge=v2rvi_vahetus(ettur,(0,0,0),vahe_v2rv)
        valge=v2rvi_vahetus(valge,(255,255,255),tuple(int(kast.tekst) for kast in v2rv_list[3]))
        valge=v2rvi_vahetus(valge,vahe_v2rv,tuple(int(kast.tekst) for kast in v2rv_list[2]))
        valge.set_colorkey(praak_v2rv)
        aken.blit(valge,(20,aknake-220))
        
        #Redaktori mabendivaliku "_" näide
        pygame.draw.line(aken,tuple(int(kast.tekst) for kast in v2rv_list[7]),(363,aknake-220+3),(437,aknake-220+77),3)
        pygame.draw.line(aken,tuple(int(kast.tekst) for kast in v2rv_list[7]),(363,aknake-220+77),(437,aknake-220+3),3)
        pygame.draw.rect(aken,tuple(int(kast.tekst) for kast in v2rv_list[4]),(20,aknake-220,320,160),1)
        
        #Raam
        pygame.draw.rect(aken,tuple(int(kast.tekst) for kast in v2rv_list[4]),(20,aknake-220,320,160),1)
        
        pygame.display.flip()

        
#Üldine sätete menüü 
def sätte_menüü():

    font=pygame.font.SysFont(Kirjastiil, fondisuurus)
    
    #Sätted, mida muuta võiks saada muuta: Logid, fondisuurus(hoiatus), pealkirja suurus,Tähestik,edasi-tagasi nuppude klahvid, mabendi-rada,mabendite list,tähestik
    #s2tte_muutus={"logid":logid, "fondisuurus":str(fondisuurus), "nime_fondisuurus":str(nime_fondisuurus),"mabendi_rada":mabendi_rada,"mabendi_rada":", ".join(list(pygame.key.name(klahv) for klahv in tagasi_klahvid)), "edasi_klahvid":", ".join(list(pygame.key.name(klahv) for klahv in edasi_klahvid)),"Tähestik":", ".join(list(map(str,Tähestik)))}
    #väärtused=list(s2tte_muutus.values())
    kirjeldus=["Kas salvestada tegevustest logisid?","Fondisuurus(Muuda pigem vaid 30-st väiksemaks)","Redaktoris faili nime fondisuurus","Rada mabendite kaustani","Mis klahvidega käik tagasi?","Mis klahvidega käik edasi?","Tähestik koordinaatide jaoks","Kuidas tähistada valitud ruute?"]
    
    all_22r=60
    #Menüü nupud
    Tagasi=Nupp("Tagasi","Tagasi",420,aknake-all_22r)
    Lähtesta=Nupp("Lähtesta","Lähtesta",540,aknake-all_22r)
    Rakenda=Nupp("Rakenda","Rakenda",700,aknake-all_22r)
    v2rv_s2te=Nupp("Värvide sätted","Värvide sätted",20,aknake-all_22r)
    
    nupud=[v2rv_s2te,Rakenda,Tagasi,Lähtesta]

    
    #Sätete nupud
    logi_nupp=Lyliti("logid",550,30,valitud=logid)
    fondi_nupp=Tekstikast("fondisuurus",str(fondisuurus),700,115,minlaius=60,maks="80",lubat=numbri_klahvid)
    pea_fondi_nupp=Tekstikast("nime_fondisuurus",str(nime_fondisuurus),500,220,minlaius=60,maks="160",lubat=numbri_klahvid)
    raja_nupp=Tekstikast("mabendi_rada",mabendi_rada,400,315,minlaius=100)
    tagasi_nupp=Tekstikast("tagasi_klahvid",",".join(list(pygame.key.name(klahv) for klahv in tagasi_klahvid)[:-1]),400,415,minlaius=100)
    edasi_nupp=Tekstikast("edasi_klahvid",",".join(list(pygame.key.name(klahv) for klahv in edasi_klahvid)[:-1]),400,515,minlaius=100)
    t2he_nupp=Tekstikast("Tähestik",",".join(list(map(str,Tähestik))),20,660,minlaius=300)#,fondisuurus=fondisuurus//2)
    
    pilt_valik=Valik("val_pilt",["Värv","Nurk"],["Värv","Nurk"],120,780,vahe=200,valitud=val_pilt)
    
    s2ttenupud=[logi_nupp,fondi_nupp,pea_fondi_nupp,raja_nupp,tagasi_nupp,edasi_nupp,t2he_nupp,pilt_valik]
    #Kui vaja, v2hendab reavahet
    if aknake<800:
        a=0
        s2ttenupud[0].muudatus(x=20)
        for nupp in s2ttenupud[1:-2]:
            nupp.muudatus(x=s2ttenupud[a].x+60)
            a+=1
        s2ttenupud[0].muudatus(x=30)
        s2ttenupud[-2].muudatus(x=s2ttenupud[a].x+100)
        pilt_valik.muudatus(x=s2ttenupud[a+1].x+100)
        
    Valitud=None
    Muut=False
    L2ht=False
    Rakenda.t66t=Muut
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                return 3
            elif event.type == KEYDOWN:
                #Lahkub mängust, kui ESC 
                if event.key == K_ESCAPE:
                    #Et ei läheks muuga sassi
                    return(2)
                #Kirjutab ainult siis, kui mingi tekstikast on valitud
                if Valitud!=None:
                    if event.key in (K_RETURN,K_TAB):
                        s2ttenupud[Valitud].valitud=False
                        #Kui shifti vajutatakse 
                        if event.mod & pygame.KMOD_SHIFT:
                            #Võtab eelmise kasti lahti
                            if Valitud==1:
                                Valitud=None
                            else:
                                Valitud-=1
                                s2ttenupud[Valitud].valitud=True
                                s2ttenupud[Valitud].just=True
                        else:                            
                            #Võtab järgmise kasti lahti
                            if Valitud==6:
                                Valitud=None
                            else:
                                Valitud+=1
                                s2ttenupud[Valitud].valitud=True
                                s2ttenupud[Valitud].just=True
                    else:
                        s2ttenupud[Valitud].sisend(event)
                        Muut=True
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                #Vaatab, kas menüü nuppudele on vajutatud
                if v2rv_s2te.h6ljund:
                    vastus=v2rvi_menüü()
                    if vastus==3:
                        return 3
                if Tagasi.h6ljund:
                    return 2
                #Rakendab sätted
                if Rakenda.h6ljund:
                    s2tte_muutus={}
                    for s2te in s2ttenupud[:-2]:
                        s2tte_muutus[s2te.nimi]=s2te.tekst
                    s2tte_muutus["logid"]=str(logi_nupp.v22rtus)
                    s2tte_muutus["mabendi_rada"]=s2tte_muutus["mabendi_rada"]
                    s2tte_muutus["tagasi_klahvid"]="[K_"+",K_".join(s2tte_muutus["tagasi_klahvid"].split(","))+",K_RIGHT]"
                    s2tte_muutus["edasi_klahvid"]="[K_"+",K_".join(s2tte_muutus["edasi_klahvid"].split(","))+",K_RIGHT]"
                    s2tte_muutus["Tähestik"]='["'+'","'.join(s2tte_muutus["Tähestik"].split(","))+'"]'
                    s2tte_muutus["val_pilt"]='"'+pilt_valik.v22rtus+'"'
                    uuenda_s2tted(s2tte_muutus)
                    
                    Muut=False
                    
                if Lähtesta.h6ljund:
                    if valik("Kas sa oled kindel?",["Jah","Ei"])=="Jah":
                        L2ht=True
                 
                #Vaatab, kas sätete kastidele on vajutatud, kui on, siis valib need
                if Valitud:
                    s2ttenupud[Valitud].valitud=False
                if logi_nupp.h6ljund:
                    logi_nupp.v22rtus= not logi_nupp.v22rtus
                    Valitud=None
                elif pilt_valik.h6ljund:
                    Muut=True
                    pilt_valik.klikk()
                    Valitud=None
                else:
                    Valitud=None
                for nupp in range(1,7):
                    if s2ttenupud[nupp].h6ljund:
                        Valitud=nupp
                        break
                if Valitud:
                    s2ttenupud[Valitud].valitud=True
            aken.fill(tausta_v2rv)
            pygame.draw.rect(aken,teksti_v2rv,(10,10,laua_laius+äär*2-20,aknake-80),1)        
            for r in range(len(kirjeldus)-1):
                #Kirjutab sätete kirjeldused 
                aken.blit(font.render(kirjeldus[r],True,teksti_v2rv),(20,20+(60 if aknake<800 else 100)*r))
            aken.blit(font.render(kirjeldus[-1],True,teksti_v2rv),(20,pilt_valik.x-40))
            for nupp in (nupud+s2ttenupud):
                nupp.hiir()
            Rakenda.t66t=Muut
            
            if L2ht==True:
                L2ht=False
                #Paneb kõik algsed sätted listi
                alg=open("algsed_s2tted.py","r",encoding="UTF-8")
                s2t=[]
                for i in alg:
                    s2t.append(i)
                alg.close()
                #Viib algsätted sätete faili
                uus=open("s2tted.py","w",encoding="UTF-8")
                for i in s2t:
                    uus.write(i)
                uus.close()
                
                #Lähtestab nuppude väärtused
                logi_nupp.v22rtus=(logid)
                fondi_nupp.tekst=str(fondisuurus)
                pea_fondi_nupp.tekst=str(nime_fondisuurus)
                raja_nupp.tekst=mabendi_rada
                tagasi_nupp.tekst=",".join(list(pygame.key.name(klahv) for klahv in tagasi_klahvid[:-1]))
                edasi_nupp.tekst=",".join(list(pygame.key.name(klahv) for klahv in edasi_klahvid[:-1]))
                t2he_nupp.tekst=",".join(list(map(str,Tähestik)))
                pilt_valik.sea("Nurk")
                
                Muut=False
                teade("Vajadusel sule ja ava mäng uuesti",1000)
            
            pygame.display.flip()
            #Kivi-paber-käärid, kui tahad tagasi võtta
