from mabe_redaktor import *
from mabe_mäng import *
from sätte_funkt import *
from teave import *
from random import randint

def avaleht():
    #Teeb tausta jaoks laua ja nuppude pildid
    read=14
    veerud=12
    laud=[]
    laud.append(("S "*(veerud+1)+"S").split(" "))
    for i in range(read):
        laud.append(("S "+"_ "*(veerud)+"S").split(" "))
    laud.append(("S "*(veerud+1)+"S").split(" "))
    
    tausta_mabendid=(mabendijoonised_s6nastikku(80))
    
    #teeb 3-20 suvalist nuppu
    for i in range(randint(10,20)):
        jupp=mabendid[randint(1,len(mabendid)-1)]
        #Suvaliselt must/valge
        if randint(1,2)==1 and jupp!="S":
            laud[randint(1,14)][randint(1,12)]=jupp.lower()
        else:
            laud[randint(1,14)][randint(1,12)]=jupp
    
    #Nupud menüüs
    m2ng=Nupp("","Uus mäng",200,300)
    lae_m2ng=Nupp("","Jätka mängu",200,350)
    redaktor=Nupp("","Ava redaktor",200,400)
    ava_s2tted=Nupp("","Sätted",200,450)
    teave=Nupp("","Teave",200,500)
    lahkumis_nupp=Nupp("","Lahku",200,550)
    
    nupud=[m2ng,lae_m2ng,redaktor,ava_s2tted,teave,lahkumis_nupp]
    
    #Peab vaatama, kuidas kontrollida, kas mängulaud olemas
    #Kui Viimatine olemas
    if "Viimatine.txt" in os.listdir("Kaardid\\"):
        Viimatine=True
    else:
        Viimatine=False
        lae_m2ng.t66t=False

    #Font mängu pealkirja jaoks
    Pealkirjafont=pygame.font.SysFont(Kirjastiil, 200)
    
    väike_font=pygame.font.SysFont(Kirjastiil, fondisuurus//2)
    
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui aken kinni nupp
            if event.type == QUIT:                
                pygame.quit()
                return        
            #Klahvivajutused
            elif event.type == KEYDOWN:
                #Avab lahkumise menüü kui ESC
                if event.key == K_ESCAPE:
                    lahku=valik("Lahku?",["Jah","Ei"])
                    if lahku=="Jah":
                        pygame.quit()
                        return
            #Kui hiireklikk    
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                #Salvestab mängu
                if m2ng.h6ljund:
                    if Viimatine:
                        os.remove("Kaardid\\Viimatine.txt")
                    vastus=Mäng(None)
                    if vastus==3:
                        pygame.quit()
                        return
                    if "Viimatine.txt" in os.listdir("Kaardid\\"):
                        Viimatine=True
                    else:
                        Viimatine=False
                    lae_m2ng.t66t=Viimatine
                #Kui on varem mäng salvestatud
                if lae_m2ng.h6ljund and Viimatine:
                    Mäng(laud_failist("Viimatine"))
                    if "Viimatine.txt" in os.listdir("Kaardid\\"):
                        Viimatine=True
                    else:
                        Viimatine=False
                    lae_m2ng.t66t=Viimatine
                #Tagasi
                if redaktor.h6ljund:
                    vastus=mabe_redaktor()

                    if vastus==3:
                        pygame.quit()
                        return
                #Edasi
                if ava_s2tted.h6ljund:
                    vastus=sätte_menüü()
                    if vastus==3:
                        pygame.quit()
                        return

                if teave.h6ljund:
                    vastus=Teave()
                    if vastus==3:
                        pygame.quit()
                        return
                if lahkumis_nupp.h6ljund:
                    pygame.quit()
                    return

        #Joonistab taustale mängulaua
        mabelaud(laud,0,0, suurus=1120)
        mabendid_lauale(laud,tausta_mabendid,0,0,suurus=1120)
        
        #Joonistab menüü kasti
        pygame.draw.rect(aken,tausta_v2rv,(180,100,500,aknake-äär-100))
        pygame.draw.rect(aken,teksti_v2rv,(180,100,500,aknake-äär-100),1)
        
        #Kas "Lae Mäng" peaks hall olema
        if not Viimatine:
            lae_m2ng.t66t=False
            
        #Joonistab nupud, kui hiir peal ja mitte hall, siis raam ka
        for nupp in nupud:
            nupp.hiir()
      
        #Joonistab pealkirja
        aken.blit(Pealkirjafont.render("Mabe", True, teksti_v2rv),(180,80))
        #Looja nimi
        tiiter=väike_font.render("Loodud Harald Jõgi poolt",True,teksti_v2rv)
        aken.blit(tiiter,(680-tiiter.get_width()-5,aknake-äär-tiiter.get_height()-5))
        pygame.display.flip()

avaleht()