from mabe_funkt import *



#See osa koodist on kohandatud lehelt
#https://pythonprogramming.altervista.org/swap-color-palette-in-pygame-dafluffypotato/?doing_wp_cron=1659790913.4704630374908447265625
#Proovisin seletada, mis aru sain
def v2rvi_vahetus(pind, vana, uus):
    #Teeb uue pinna ja täidab antud värviga
    ymber = pygame.Surface(pind.get_size())
    ymber.fill(uus)
    #Muudab vana värvi vanal pinnal nähtamatuks
    pind.set_colorkey(vana)
    #Kannab vana pildi uue peale
    ymber.blit(pind, (0, 0))
    return ymber

#Joonistab antud kohta antud värviga ruudu
def ruudujoonis(rida,veerg, v2rv,ruudupikkus,vahe_vasak,vahe_peal):
    pygame.draw.rect(aken,v2rv,(vahe_vasak+veerg*ruudupikkus,vahe_peal+rida*ruudupikkus, ruudupikkus, ruudupikkus))
    
#Märgitud ruudul valitud ruudu graafika
def valitud_ruut(rida,veerg,v2rv,ruudupikkus,vahe_vasak,vahe_peal,j2medus):
    #Vastav ruut värvitud
    if val_pilt=="Värv":
        pygame.draw.rect(aken,v2rv,(vahe_vasak+veerg*ruudupikkus,vahe_peal+rida*ruudupikkus, ruudupikkus, ruudupikkus))
    #Ruudu servades nurgad
    if val_pilt=="Nurk":
        #Õiget värvi
        v2rv=mabendi_v2rv_2 if rida%2==veerg%2 else mabendi_v2rv_1
        
        padding=max(ruudupikkus//20,3)
        iks=vahe_peal+rida*ruudupikkus
        igrek=vahe_vasak+veerg*ruudupikkus
        #Joonistab loodenurga
        pygame.draw.line(aken,v2rv,(igrek+padding,iks+padding),(igrek+padding+ruudupikkus//4,iks+padding),j2medus)
        pygame.draw.line(aken,v2rv,(igrek+padding,iks+padding),(igrek+padding,iks+padding+ruudupikkus//4),j2medus)
        
        #Joonistab kirdenurga
        pygame.draw.line(aken,v2rv,(igrek+ruudupikkus-padding,iks+padding),(igrek+ruudupikkus-padding-ruudupikkus//4,iks+padding),j2medus)
        pygame.draw.line(aken,v2rv,(igrek+ruudupikkus-padding,iks+padding),(igrek+ruudupikkus-padding,iks+padding+ruudupikkus//4),j2medus)
        
        #Joonistab edelanurga
        pygame.draw.line(aken,v2rv,(igrek+padding,iks+ruudupikkus-padding),(igrek+padding+ruudupikkus//4,iks+ruudupikkus-padding),j2medus)
        pygame.draw.line(aken,v2rv,(igrek+padding,iks+ruudupikkus-padding),(igrek+padding,iks+ruudupikkus-padding-ruudupikkus//4),j2medus)
        
        #Joonistab kagunurga
        pygame.draw.line(aken,v2rv,(igrek+ruudupikkus-padding,iks+ruudupikkus-padding),(igrek+ruudupikkus-padding-ruudupikkus//4,iks+ruudupikkus-padding),j2medus)
        pygame.draw.line(aken,v2rv,(igrek+ruudupikkus-padding,iks+ruudupikkus-padding),(igrek+ruudupikkus-padding,iks+ruudupikkus-padding-ruudupikkus//4),j2medus)


#Teeb sõnastiku mabendite joonistest, mida saab siis kohtadesse panna
def mabendijoonised_s6nastikku(ruudupikkus):
    s6nastik={}
    #Teeb 'S' ära
    sein=pygame.Surface((ruudupikkus,ruudupikkus))
    sein.fill(seina_v2rv)
    s6nastik["S"]=sein
    #Jätab '_' ja 'S' vahele
    for mabend in mabendid[2:]:
        
        mabend_pilt=pygame.Surface((80,80))
        #Muudab tausta, et ei oleks mustaga sekkuv
        mabend_pilt.fill(praak_v2rv)
        mabend_pilt.blit(pygame.image.load(os.path.join("Pildid",mabendi_rada,mabend+".png")),(0,0))
        #Õige suurus
        mabend_pilt=pygame.transform.scale(mabend_pilt,(ruudupikkus,ruudupikkus))
        
        #Teeb musta versiooni mabendist ja lisab sõnastikku
        must=v2rvi_vahetus(mabend_pilt,(0,0,0),vahe_v2rv)
        must=v2rvi_vahetus(must,(255,255,255),mabendi_v2rv_1)
        must=v2rvi_vahetus(must,vahe_v2rv,mabendi_v2rv_2)
        must.set_colorkey(praak_v2rv)
        s6nastik[mabend.lower()]=must
        
        #Teeb valge versiooni mabendist ja lisab sõnastikku
        valge=v2rvi_vahetus(mabend_pilt,(0,0,0),vahe_v2rv)
        valge=v2rvi_vahetus(valge,(255,255,255),mabendi_v2rv_2)
        valge=v2rvi_vahetus(valge,vahe_v2rv,mabendi_v2rv_1)
        valge.set_colorkey(praak_v2rv)
        s6nastik[mabend.upper()]=valge
        #s6nastik[mabend.upper()]=mabend_pilt
    return(s6nastik)
    

#Joonistab malelauamustriga ruudustiku. Ruudud on ruutude arv.
def mabelaud(laud,vahe_vasak,vahe_peal,fondisuurus=fondisuurus, suurus=laua_laius):
    global aken
    read=len(laud)-2
    veerud=len(laud[0])-2
    ruudupikkus=int(suurus/max(read,veerud))

    
    
    pool=1
    v2rv_t6lk=['0',v2rv_1,v2rv_2]
    #Koordinaatide jaoks
    aken.fill((v2rv_2))
    #Joonistab ruudud
    for r in range (read):
        for v in range(veerud):
            #pygame.draw.rect(aken,v2rv_t6lk[pool],(vahe_vasak+v*ruudupikkus,vahe_peal+r*ruudupikkus, ruudupikkus, ruudupikkus))
            ruudujoonis(r,v,v2rv_t6lk[pool],ruudupikkus,vahe_vasak,vahe_peal)
            pool*=-1
        if veerud%2==0:
            pool*=-1
    #Joonistab ääred lauale
    pygame.draw.rect(aken,v2rv_1,(vahe_vasak-3,vahe_peal-3,veerud*ruudupikkus+6,read*ruudupikkus+6),3)
    #Teeb laua äärtele numbrid ja tähed koordinaatide jaoks
    font = pygame.font.SysFont(Kirjastiil, fondisuurus)
    for v in range(veerud):       
        t2ht = font.render(Tähestik[v], True, teksti_v2rv)
        aken.blit(t2ht, (vahe_vasak+ruudupikkus/2-10+v*ruudupikkus, vahe_peal-40))
        aken.blit(t2ht, (vahe_vasak+ruudupikkus/2-10+v*ruudupikkus, vahe_peal+10+read*ruudupikkus))
           
    for r in range(read):
        arv = font.render(str(r+1), True, teksti_v2rv)
        #Et vasakpoolsed numbrid kahekohalisena sassi ei läheks
        if r<9:
            aken.blit(arv, (vahe_vasak-23, vahe_peal+ruudupikkus/2-15+r*ruudupikkus))
        else:
            aken.blit(arv, (vahe_vasak-38, vahe_peal+ruudupikkus/2-15+r*ruudupikkus))
        aken.blit(arv, (vahe_vasak+10+veerud*ruudupikkus, vahe_peal+ruudupikkus/2-15+r*ruudupikkus))
               
#Joonistab lauale laua tabelist nupud
def mabendid_lauale(laud,mabendi_s6nastik,vahe_vasak,vahe_peal,suurus=laua_laius,t2htsad=[]):
    read=len(laud)-2
    veerud=len(laud[0])-2
    ruudupikkus=int(suurus/max(read,veerud))
    for r in range(1,read+1):
        for v in range(1,veerud+1):
            #Kui pole tühi koht
            if laud[r][v]!="_":
                if (r,v) in t2htsad:
                    temp_pilt=mabendi_s6nastik[laud[r][v]].copy()
                    temp_pilt=v2rvi_vahetus(temp_pilt, mabendi_v2rv_1 if kumb_pool(laud[r][v])==1 else mabendi_v2rv_2, t2ht_v2rv)
                    temp_pilt.set_colorkey(praak_v2rv)
                    aken.blit(temp_pilt,((v-1)*ruudupikkus+vahe_vasak,(r-1)*ruudupikkus+vahe_peal))
                else:
                    aken.blit(mabendi_s6nastik[laud[r][v]],((v-1)*ruudupikkus+vahe_vasak,(r-1)*ruudupikkus+vahe_peal))
            if False:  
                #Kui pole tühi ruut
                if laud[r][v]!="_":
                    #Kui on läbimatu
                    if laud[r][v]=="S":
                        ruudujoonis(r-1,v-1,seina_v2rv,ruudupikkus,vahe_vasak,vahe_peal)
                    else:
                        mabendijoonis(laud[r][v],kumb_pool(laud[r][v]),r,v,ruudupikkus,vahe_vasak,vahe_peal)


#Joonistab õigesse kohta nupu, millel on peal selle nimetäht. (Kuna malenuppu kutsutakse malendiks, siis mabenupp on mabend)
def mabendijoonis(mabend,pool,rida,veerg,ruudupikkus,vahe_vasak, vahe_peal):
    font = pygame.font.SysFont(Kirjastiil, int(ruudupikkus/2))
    
    v1=v2rv_1
    v2=v2rv_2

    #Kui must
    if pool==1:
        v1=v2rv_2
        v2=v2rv_1

    #Joonistab ringi
    pygame.draw.circle(aken,v1,(int((veerg-0.5)*ruudupikkus+vahe_vasak),int((rida-0.5)*ruudupikkus+vahe_peal)),int(ruudupikkus/2-2))
    pygame.draw.circle(aken,v2,(int((veerg-0.5)*ruudupikkus+vahe_vasak),int((rida-0.5)*ruudupikkus+vahe_peal)),int(ruudupikkus/2-5))
    #Kui kabenupp
    if mabend=="N" or mabend=='n':
        return
    
    Kiri=font.render(mabend.lower().capitalize(), True, v1)
    aken.blit(Kiri, (int((veerg-0.5)*ruudupikkus+vahe_vasak-ruudupikkus/2*len(mabend)/5),int((rida-0.5)*ruudupikkus+vahe_peal-ruudupikkus/7)))

    

    