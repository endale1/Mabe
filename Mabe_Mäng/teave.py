from mabe_klassid import *

def demline(x,nimi,font):
    pygame.draw.line(aken,teksti_v2rv,(0,x),(1000,x))
    aken.blit(font.render(nimi,True,teksti_v2rv),(20,x+5))
#Teeb pildid yhe lehekylje jaoks
def lehe_pilt(leht,reavahe=5):
    font = pygame.font.SysFont(Kirjastiil, 20)
    #J2rjend piltidest
    lehekylg=[]
    #Ridade k6rgused
    koht=[]
    
    eelmine=0
    for rida in leht:
        if type(rida)==str:
            lehekylg.append(font.render(rida,True,teksti_v2rv))
            koht.append([10,eelmine+reavahe])
            eelmine+=lehekylg[-1].get_height()
        elif type(rida)==list:
            lehekylg.append(rida[0])
            koht.append([rida[2],eelmine+reavahe])
            eelmine+=rida[1]
        
    
    return(lehekylg,koht)
    
def Teave():
    font = pygame.font.SysFont(Kirjastiil, 20)
    Pea_font = pygame.font.SysFont(Kirjastiil, nime_fondisuurus)
    
    Tekst=open("Teabe_tekst.txt","r",encoding="UTF-8")
    Lehed=[]
    Peatykid=[]
    leht=[]
    for rida in Tekst:
        if rida[0]==">":
            if Peatykid!=[]:
                Lehed.append(leht)
                leht=[]
            Peatykid.append(rida[1:].strip("\n"))
        #Kui tegu on pildiga
        elif rida[0]=="[":
            info=rida.strip()[1:-1].split(";")
            pind=pygame.image.load(os.path.join("Pildid",info[0]))
            pind=pygame.transform.scale(pind,(int(info[1]),int(info[2])))
            pygame.draw.rect(pind,teksti_v2rv,(0,0,int(info[1]),int(info[2])),1)
            leht.append([pind,int(info[-2]),20 if len(info)==3 else int(info[4])])
        else:
            leht.append(rida.strip("\n"))
    Tekst.close()
    Peatykid.pop(len(Peatykid)-1)
    Lehekylg=0
    
    #Nupud
    Lahku=Nupp("Lahku","Lahku",750,aknake-50)
    Eel=Nupp("Eelmine","<-",60,aknake-120)
    Järg=Nupp("Järgmine","->",150,aknake-120)
    
    K=14*43+2+20
    #Scrollbar
    eel=Nupp("Eelmine","▲",0,20,raam=True,fondisuurus=10)
    j2r=Nupp("Järgmine","▼",0,0,raam=True,fondisuurus=10)
    j2r.muudatus(x=K-j2r.k6rgus,y=860-j2r.laius)
    eel.muudatus(y=860-j2r.laius)
    
    pea_nupud=Pikk_Valik("Peatükid",Peatykid,Peatykid,20,20,200,suund="vert",k6rgus=12)
    nupud=[Lahku,Eel,Järg,pea_nupud,eel,j2r]
    
    Ribikkus=K-20-eel.k6rgus-j2r.k6rgus
    
    K6RGUS=0
    tekst,koht=lehe_pilt(Lehed[Lehekylg])
    lehesuurus=koht[-1][1]+tekst[-1].get_height()+5
    #Lehekylje ja scrollbari pikkuste suhe
    Lehesuhe=round((K-120)/lehesuurus,6)
    Riba=pygame.Surface((eel.laius,Ribikkus*Lehesuhe))
    Riba.fill(hall)
    pygame.draw.rect(Riba,teksti_v2rv,(0,0,eel.laius,Riba.get_height()),1)
    Muut=False
    R=pygame.Rect(0,0,0,0)
    Alg=False #Kui on olemas, siis algne k6rgus-algne hiirepos
    samm=10
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui aken kinni nupp
            if event.type == QUIT:
                return 3
                
            #Klahvivajutused
            elif event.type == KEYDOWN:
                #Avab lahkumise men kui ESC
                if event.key == K_ESCAPE:
                    return 2
                
                #Vahetab lehekülge, kui vaja
                if event.key in (K_RETURN,K_TAB):                
                    #Kui shifti vajutatakse
                    if event.mod & pygame.KMOD_SHIFT and Lehekylg!=0:
                        Lehekylg-=1
                        Muut=True
                    elif Lehekylg!=len(Lehed)-1:
                        Lehekylg+=1
                        Muut=True
                if event.key == K_LEFT and Lehekylg!=0:
                    Lehekylg-=1
                    Muut=True
                elif event.key == K_RIGHT and Lehekylg!=len(Lehed)-1:                            
                    Lehekylg+=1
                    Muut=True
                #Liigub yles-alla
                elif event.key == K_UP:
                    K6RGUS=max(K6RGUS-samm,0)
                elif event.key == K_DOWN:
                    K6RGUS=min(K6RGUS+samm,lehesuurus-(K-120))
                pea_nupud.valitud=Lehekylg
                    
            #Kui hiireklikk    
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                if Lahku.h6ljund:
                    return 2
                if Eel.h6ljund:
                    Lehekylg-=1
                    Muut=True
                if Järg.h6ljund:
                    Lehekylg+=1
                    Muut=True
                pea_nupud.valitud=Lehekylg
                if pea_nupud.h6ljund:
                    pea_nupud.klikk()
                    Lehekylg=pea_nupud.valitud
                    Muut=True
                
                if R.collidepoint(pygame.mouse.get_pos()):
                    Alg=int(K6RGUS/lehesuurus*Ribikkus)-pygame.mouse.get_pos()[1]+1
            elif event.type == MOUSEWHEEL:
                K6RGUS=max(min(K6RGUS-event.y*samm*2,lehesuurus-(K-120)),0)
        #Kas muudatus
        if pygame.mouse.get_pressed()[0] and K-120<lehesuurus:
            if eel.h6ljund:
                K6RGUS=max(K6RGUS-samm,0)
            elif j2r.h6ljund:
                K6RGUS=min(K6RGUS+samm,lehesuurus-(K-120))
            elif Alg:
                K6RGUS=max(min(int((pygame.mouse.get_pos()[1]+Alg)/Ribikkus*lehesuurus),lehesuurus-(K-120)),0)
        else:
            Alg=False
        R=R.move(0,20+eel.k6rgus+int(K6RGUS/lehesuurus*Ribikkus)-R[1])
        if Muut:
            K6RGUS=0
            tekst,koht=lehe_pilt(Lehed[Lehekylg])
            lehesuurus=koht[-1][1]+tekst[-1].get_height()+5
            #Lehekylje ja scrollbari pikkuste suhe
            Lehesuhe=round((K-120)/(lehesuurus),6)
            Ribikkus=K-20-eel.k6rgus*2+2
            Riba=pygame.Surface((eel.laius,Ribikkus*Lehesuhe))
            Riba.fill(hall)
            pygame.draw.rect(Riba,teksti_v2rv,(0,0,eel.laius,Riba.get_height()),1)
            R=pygame.Rect(eel.y,20+eel.k6rgus,Riba.get_width(),Riba.get_height())
            Muut=False
        Eel.t66t=Lehekylg!=0
        Järg.t66t=Lehekylg!=len(Lehed)-1
        eel.t66t=K6RGUS!=0
        j2r.t66t=K6RGUS<lehesuurus-(K-120)
        
        #Joonistab asjad
        aken.fill(tausta_v2rv)
        #Kirjutab teksti ja pildid
        Vahe=pygame.Surface((640,K-120))
        Vahe.fill(tausta_v2rv)
        for rida in range(len(tekst)):
            Vahe.blit(tekst[rida],(koht[rida][0],koht[rida][1]-K6RGUS))
        aken.blit(Vahe,(220,120))
        pygame.draw.rect(aken,teksti_v2rv,(20,20,840,43*14+2),1)
        pygame.draw.line(aken,teksti_v2rv,(220,120),(840,120))
        for nupp in nupud:
            nupp.hiir()
            
        pygame.draw.line(aken,teksti_v2rv,(eel.y,eel.x+eel.k6rgus),(j2r.y,j2r.x))    
        
        nimi=Pea_font.render(pea_nupud.nupulist[Lehekylg].tekst,True,teksti_v2rv)
        aken.blit(nimi,(220+(640-nimi.get_width())//2,40))
        #Scrollbar
        if K-120<lehesuurus:
            aken.blit(Riba,(eel.y,20+eel.k6rgus+int(K6RGUS/lehesuurus*Ribikkus)))
            
        #Jooned
        #demline(K,"k",font)
        #demline(120,"Teksti algus",font)
        #demline(120+lehesuurus-K6RGUS,"Lehe piir",font)
        
        pygame.display.flip()
        clock.tick(40)