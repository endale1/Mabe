from mabe_klassid import *
from menüü_funkt import *
from graafika_funkt import *

def uus_m2ng():
    laud=laud_failist("Male")[0]
    #t2ht_k2s: v6i-kui üks tüüp sureb,mäng läbi; ja- kui kõik surevad, mäng läbi, ei-Tähtsad ei ole tähtsad. 
    omadused=copy.deepcopy(vaike_omadused)
    font=pygame.font.SysFont(Kirjastiil, int(fondisuurus//1.5))
    
    Valmis=Nupp("Valmis","Valmis",750,aknake-50)
    Tagasi=Nupp("Tagasi","Tagasi",600 if Suur else 750,aknake-(50 if Suur else 100))
    Salvesta=Nupp("Salvesta","Salvesta",750,aknake-150)
    #Tähtsad nupud
    Valitsus=Nupp("Valitsus","Kuninglikud nupud",10,620)
    #Edutamise valikud
    Edu=Nupp("Edu","Edutused",10,680)
    menüü_nupud=[Valmis,Tagasi,Salvesta,Valitsus,Edu]
    
    v6tmed=list(omadused.keys())[:4]
    #Jah/ei reeglid
    s2tte_nupud=[Lyliti(v6tmed[omadus],240,353+omadus*60,valitud=omadused[v6tmed[omadus]]) for omadus in range(4)]
    #Võidutingimus
    v6idutus=Valik("t2ht_k2s",["ja","v6i","ei"],["Tapa kõik kuninglikud nupud","Tapa üks kuninglik nupp","Tapa kõik vaenlase nupud"],
                             300,640,valitud=omadused["t2ht_k2s"],fondisuurus=int(fondisuurus/1.5),suund="vert",vahe=5)
    s2tte_nupud.append(v6idutus)
    
    #List olemasolevatest kaartidest
    kaardid=os.listdir("Kaardid")
    for kaart in range(len(kaardid)):
        #Võtab laiendi lõpu ära
        kaardid[kaart]=kaardid[kaart][:-4]
        
    #Kaardide listist valimine
    kaardi_nupud=Pikk_Valik("Kaart",kaardid,kaardid,20,20,240,suund="vert",k6rgus=5)
    kaardi_nupud.valitud=kaardi_nupud.leia("Male")
    laud=laud_failist(kaardi_nupud.nupulist[kaardi_nupud.valitud].nimi)[0]
    
    kirjeldused=["Sundvõtmine","Käigu vahele jätmine","En Passant","Vangerdus","Milline on mängu võidutingimus?"]
    suurus=60
    laius=8
    k6rgus=8
    mabendi_s6nastik=(mabendijoonised_s6nastikku(suurus))
    
    #Valitud=None
    
    #Täidab tühjad kohad edutustes
    M=mabendid[1:]
    for Mab in M+list(mabend.lower() for mabend in M):
        if Mab not in list(omadused["edutused"].keys()):
            omadused["edutused"][Mab]=[]
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui aken kinni nupp
            if event.type == QUIT:
                return 3           
            #Klahvivajutused
            elif event.type == KEYDOWN:
                #Avab lahkumise men kui ESC
                if event.key == K_ESCAPE:
                    return 2
            #Kui hiireklikk    
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                if Tagasi.h6ljund:
                    return 2
                if Valmis.h6ljund:
                    for nupp in s2tte_nupud:
                        omadused[nupp.nimi]=nupp.v22rtus
                    return(laud,omadused)
                if Salvesta.h6ljund:
                    
                    if kaardi_nupud.nupulist[kaardi_nupud.valitud].nimi not in alg_lauad:
                        if valik("Kas sa oled kindel?",["Jah","Ei"])=="Jah":
                            
                            for nupp in s2tte_nupud:
                                omadused[nupp.nimi]=nupp.v22rtus
                            ed_back=copy.deepcopy(omadused["edutused"])
                            for nupp in list(omadused["edutused"].keys()):
                                if omadused["edutused"][nupp]==[]:
                                    del omadused["edutused"][nupp]
                            laud_faili(kaardi_nupud.nupulist[kaardi_nupud.valitud].nimi,laud,omadused)
                            omadused["edutused"]=ed_back
                            teade("Salvestatud",1000)
                    else:
                        teade("Seda mängulauda ei või muuta",1000)
                #Kuninglikud nupud
                if Valitsus.h6ljund:
                    K=kuningad(omadused["t2htis"],laud)
                    if K==3:
                        return 3
                    omadused["t2htis"]=K
                #Kuninglikud nupud
                if Edu.h6ljund:
                    E=Edu_s2tted(omadused["edutused"])
                    if E==3:
                        return 3
                    omadused["edutused"]=E
                #Kaartide loeng
                if kaardi_nupud.h6ljund:
                    kaardi_nupud.klikk()
                    #Kui kl6psati kaardile, mitte edasi-tagasi
                    if not kaardi_nupud.eel.h6ljund and not kaardi_nupud.j2r.h6ljund:
                        L=laud_failist(kaardi_nupud.nupulist[kaardi_nupud.valitud].nimi)
                        laud=L[0]
                        laius=len(laud[0])-2
                        k6rgus=len(laud)-2
                        #Omadused 6igeks
                        omadused=L[1]
                        v6tmed=list(omadused.keys())[:5]
                        for nupp in range(4):
                            s2tte_nupud[nupp].v22rtus=omadused[v6tmed[nupp]]
                        v6idutus.sea(omadused[v6tmed[4]])
                        for Mab in M+list(mabend.lower() for mabend in M):
                            if Mab not in list(omadused["edutused"].keys()):
                                omadused["edutused"][Mab]=[]
                            
                #Nupud
                for nupp in s2tte_nupud[:-1]+[Edu]: 
                    nupp.klikk()
                if s2tte_nupud[-1].h6ljund:
                    s2tte_nupud[-1].klikk() 
        
        #Muudab vajadusel nuppude suuruse
        if suurus!=480//max(laius,k6rgus):
            suurus=480//max(laius,k6rgus)
            mabendi_s6nastik=(mabendijoonised_s6nastikku(suurus))


        aken.fill(tausta_v2rv)
        #Näidislaud
        vahe_peal=60+(480-(k6rgus*suurus))/2
        vahe_vasak=280+60+(480-(laius*suurus))/2
        mabelaud(laud,vahe_vasak,vahe_peal,suurus=480)
        mabendid_lauale(laud,mabendi_s6nastik,vahe_vasak,vahe_peal,suurus=480)
        pygame.draw.line(aken,teksti_v2rv,(280,600),(880,600),3)
        pygame.draw.line(aken,teksti_v2rv,(280,0),(280,600),3)
        
        #Kirjeldused:
        for kiri in range(len(kirjeldused)-1):
            aken.blit(font.render(kirjeldused[kiri],True,teksti_v2rv),(20,350+60*kiri))
        aken.blit(font.render(kirjeldused[-1],True,teksti_v2rv),(300,610))                    
        #Nupud ja tekstikastid
        for nupp in ([kaardi_nupud]+menüü_nupud+s2tte_nupud):
            nupp.hiir()            
        pygame.display.flip()
    return(laud,omadused)

#Kuninglikkude nuppude valimise menüü
def kuningad(t2htis,laud):
    M=mabendid[2:]
    alg=copy.deepcopy(t2htis)
    laius=len(laud[0])-2
    k6rgus=len(laud)-2
    suurus=480//max(laius,k6rgus)
    #Mabendid valiku jaoks
    nupu_mabestik=(mabendijoonised_s6nastikku(80))
    back_nupu=nupu_mabestik.copy()
    #Mabendid m2ngulaua jaoks
    mabestik=mabendijoonised_s6nastikku(suurus)
    back_mab=mabestik.copy()
    mabestik=back_mab.copy()
    vahe_peal=60+(480-(k6rgus*suurus))/2
    vahe_vasak=280+60+(480-(laius*suurus))/2
    Mablist=list(mabestik.keys())
    
    Pool=1
    
    #Muudab v22rtuslike mabendite 22rev2rvid kuldseks
    for joonis in Mablist:
        if joonis in t2htis:
            #Kui valge
            if joonis.upper()==joonis:
                mabestik[joonis]=v2rvi_vahetus(mabestik[joonis],mabendi_v2rv_1,t2ht_v2rv)
                nupu_mabestik[joonis]=v2rvi_vahetus(nupu_mabestik[joonis],mabendi_v2rv_1,t2ht_v2rv)
            else:
                mabestik[joonis]=v2rvi_vahetus(mabestik[joonis],mabendi_v2rv_2,t2ht_v2rv)
                nupu_mabestik[joonis]=v2rvi_vahetus(nupu_mabestik[joonis],mabendi_v2rv_2,t2ht_v2rv)
        mabestik[joonis].set_colorkey(praak_v2rv)
        nupu_mabestik[joonis].set_colorkey(praak_v2rv)
    #Nupud
    Valmis=Nupp("Valmis","Valmis",750,aknake-50)
    Tagasi=Nupp("Tagasi","Tagasi",600 if Suur else 750,aknake-(50 if Suur else 130))
    #Mabestike valik
    V22rt=Pikk_Valik("T2htis",M,M,20,20,80,k6rgus=6,suund="vert",Pilt=list(nupu_mabestik[Mab] for Mab in M))
    #Teeb pildid poole vahetamise nupu jaoks
    must=pygame.Surface((80,80))
    must.fill(mabendi_v2rv_1)
    pygame.draw.polygon(must,mabendi_v2rv_2,((0,0),(0,80),(80,80)))
    valge=pygame.Surface((80,80))
    valge.fill(mabendi_v2rv_2)
    pygame.draw.polygon(valge,mabendi_v2rv_1,((0,0),(0,80),(80,80)))
    Poole_pildid=[0,valge,must]
    Poole_nupp=Lyliti("Pool",20,aknake-100,valitud_raam=1,Pilt=Poole_pildid[Pool])
    #Mõlematel pooltel sama
    Peegel=Lyliti("Peegel",300,aknake-100,tekst="Mõlemal poolel sama",valitud=True)
    nupud=[Valmis,Tagasi,V22rt,Poole_nupp,Peegel]
    f1=False
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui aken kinni nupp
            if event.type == QUIT:
                return 3           
            #Klahvivajutused
            elif event.type == KEYDOWN:
                #Avab lahkumise men kui ESC
                if event.key == K_ESCAPE:
                    return(alg)
            #Kui hiireklikk    
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                #Kui klikk oli mänguvälja sees
                pos=pygame.mouse.get_pos()
                if vahe_peal<pos[1]<(k6rgus*suurus+vahe_peal) and vahe_vasak<pos[0]<(laius*suurus+vahe_vasak):
                    rida=int((pos[1]-vahe_peal)//suurus)+1
                    veerg=int((pos[0]-vahe_vasak)//suurus)+1
                    if (rida,veerg) in t2htis:
                        t2htis.remove((rida,veerg))
                        if Peegel.v22rtus and (k6rgus-rida+1,veerg) in t2htis:
                            t2htis.remove((k6rgus-rida+1,veerg))
                    else:
                        t2htis.append((rida,veerg))
                        if Peegel.v22rtus and (k6rgus-rida+1,veerg) not in t2htis:
                            t2htis.append((k6rgus-rida+1,veerg))
                    if Peegel.v22rtus:
                        k6rgus-laius
                if Tagasi.h6ljund:
                    return(alg)
                if Valmis.h6ljund:
                    return(t2htis)
                if V22rt.h6ljund:
                    V22rt.klikk()
                    if not (V22rt.eel.h6ljund or V22rt.j2r.h6ljund):
                        Muut=V22rt.nupulist[V22rt.valitud].nimi if Pool==1 else V22rt.nupulist[V22rt.valitud].nimi.lower()
                        M2=Muut.lower() if Pool==1 else Muut.upper()
                        if Muut in t2htis:
                            t2htis.remove(Muut)
                            if Peegel.v22rtus and M2 in t2htis:
                                t2htis.remove(M2)
                        else:
                            t2htis.append(Muut)
                            if Peegel.v22rtus and M2 not in t2htis:
                                t2htis.append(M2)
                        V22rt.valitud=None
                        f1=True
                if Poole_nupp.h6ljund:
                    Poole_nupp.klikk()
                    Pool*=-1
                    Poole_nupp.Pilt=Poole_pildid[Pool]
                    f1=True
                if Peegel.h6ljund:
                    Peegel.klikk()
        #Kui vaja muuta joonist
        if f1:
            f1=False
            nupu_mabestik=back_nupu.copy()
            mabestik=back_mab.copy()
            #Muudab v22rtuslike mabendite 22rev2rvid kuldseks
            for joonis in Mablist:
                if joonis in t2htis:
                    #Kui valge
                    if joonis.upper()==joonis:
                        mabestik[joonis]=v2rvi_vahetus(mabestik[joonis],mabendi_v2rv_1,t2ht_v2rv)
                        nupu_mabestik[joonis]=v2rvi_vahetus(nupu_mabestik[joonis],mabendi_v2rv_1,t2ht_v2rv)
                    else:
                        mabestik[joonis]=v2rvi_vahetus(mabestik[joonis],mabendi_v2rv_2,t2ht_v2rv)
                        nupu_mabestik[joonis]=v2rvi_vahetus(nupu_mabestik[joonis],mabendi_v2rv_2,t2ht_v2rv)
                mabestik[joonis].set_colorkey(praak_v2rv)
                nupu_mabestik[joonis].set_colorkey(praak_v2rv)
            #Muudab mabendite pildid listis
            for nupp in V22rt.nupulist:
                nupp.Pilt=nupu_mabestik[nupp.nimi if Pool==1 else nupp.nimi.lower()]
                
        #Joonistab
        aken.fill(tausta_v2rv)
        mabelaud(laud,vahe_vasak,vahe_peal,suurus=480)
        mabendid_lauale(laud,mabestik,vahe_vasak,vahe_peal,suurus=480, t2htsad=t2htis)
        pygame.draw.line(aken,teksti_v2rv,(280,600),(880,600),3)
        pygame.draw.line(aken,teksti_v2rv,(280,0),(280,600),3)
        
        
        for nupp in nupud:
            nupp.hiir()

        pygame.display.flip()
    
def Edu_s2tted(edutused):
    M=["N","T","E","V","R","O","L","K"]
    alg=copy.deepcopy(edutused)
    font=pygame.font.SysFont(Kirjastiil, fondisuurus)
    Pool=1
    #Mabendid valiku jaoks
    mabestik=(mabendijoonised_s6nastikku(80))
    #Mablist=list(mabestik.keys())
    #Nupud
    Valmis=Nupp("Valmis","Valmis",750,aknake-50)
    Tagasi=Nupp("Tagasi","Tagasi",600 if Suur else 750,aknake-(50 if Suur else 130))
    #Mabestike valik
    Edutatav=Pikk_Valik("Edutatav",M,M,80,60,680,Pilt=list(mabestik[Mab] for Mab in M),valitud=0)
    Val=Edutatav.nupulist[0].nimi
    Edu_S2tt=Pikk_Valik("Edutused",M,M,80,200,680,Pilt=list(mabestik[Mab] for Mab in M),lylitism=list(mabend in edutused[Val] for mabend in M))
    
    #Teeb pildid poole vahetamise nupu jaoks
    must=pygame.Surface((80,80))
    must.fill(mabendi_v2rv_1)
    pygame.draw.polygon(must,mabendi_v2rv_2,((0,0),(0,80),(80,80)))
    valge=pygame.Surface((80,80))
    valge.fill(mabendi_v2rv_2)
    pygame.draw.polygon(valge,mabendi_v2rv_1,((0,0),(0,80),(80,80)))
    Poole_pildid=[0,valge,must]
    Poole_nupp=Lyliti("Pool",140,aknake-400,valitud_raam=1,Pilt=Poole_pildid[Pool])
    Peegel=Lyliti("Peegel",400,aknake-400,tekst="Mõlemal poolel sama",valitud=True)
    nupud=[Valmis,Tagasi,Edutatav,Edu_S2tt,Poole_nupp,Peegel]
    
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui aken kinni nupp
            if event.type == QUIT:
                return 3           
            #Klahvivajutused
            elif event.type == KEYDOWN:
                #Avab lahkumise men kui ESC
                if event.key == K_ESCAPE:
                    return(alg)
            #Kui hiireklikk    
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                if Tagasi.h6ljund:
                    return(alg)
                if Valmis.h6ljund:
                    return(edutused)
                if Edutatav.h6ljund:
                    Edutatav.klikk()
                    if not (Edutatav.eel.h6ljund or Edutatav.j2r.h6ljund):
                        Val=Edutatav.nupulist[Edutatav.valitud].nimi
                        for nupp in Edu_S2tt.nupulist:
                            if (nupp.nimi if Pool==1 else nupp.nimi.lower()) in edutused[Val if Pool==1 else Val.lower()]:
                                nupp.v22rtus=True
                            else:
                                nupp.v22rtus=False
                #Edutuste muutmine
                if Edu_S2tt.h6ljund:
                    a=Edu_S2tt.klikk()
                    if a!=None:
                        Edu_S2tt.nupulist[a].valitud=False
                        Va=Val if Pool==1 else Val.lower()
                        a=M[a] if Pool==1 else M[a].lower()
                        if a in edutused[Va]:
                            edutused[Va].remove(a)
                        else:
                            edutused[Va].append(a)
                        
                        if Peegel.v22rtus:
                            #Kui vastaspoole omas on, aga praegusel poolel pole, kustutab vastaspoolest
                            vaat=edutused[Val if Pool==-1 else Val.lower()]
                            Peeg=a.lower() if Pool==1 else a.upper()
                            if Peeg in vaat and a not in edutused[Va]:
                                vaat.remove(Peeg)
                            elif Peeg not in vaat and a in edutused[Va]:
                                vaat.append(Peeg)
                    
                if Poole_nupp.h6ljund:
                    Poole_nupp.klikk()
                    Pool*=-1
                    Poole_nupp.Pilt=Poole_pildid[Pool]
                    for nupp in Edutatav.nupulist:
                        nupp.Pilt=mabestik[nupp.nimi if Pool==1 else nupp.nimi.lower()]
                    for nupp in Edu_S2tt.nupulist:
                        nupp.Pilt=mabestik[nupp.nimi if Pool==1 else nupp.nimi.lower()]
                        nupp.v22rtus=((nupp.nimi.lower() if Pool==-1 else nupp.nimi) in edutused[Val.lower() if Pool==-1 else Val])
                if Peegel.h6ljund:
                    Peegel.klikk()
            
        
        aken.fill(tausta_v2rv) 
        for nupp in nupud:
            nupp.hiir()
        aken.blit(font.render("Mabend, mille edutusi vahetad",True,teksti_v2rv),(400-150,20))
        aken.blit(font.render("Edutused",True,teksti_v2rv),(400-50,160))
        aken.blit(font.render("Poole vahetamine",True,teksti_v2rv),(60,aknake-450))
        pygame.display.flip()
    

#Peaks ausalt öeldes muutma
def Voit(pool):
    if "Viimatine.txt" in os.listdir("Kaardid"):
        os.remove(os.path.join("Kaardid","Viimatine.txt"))
    if pool==1:
        m2ngija="Valge"
    else:
        m2ngija="Must"
    return valik(m2ngija+" mängija võitis.",["Uus mäng", "Lahku"])

def edutamine(edu_val,mabestik,j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,edutus=None,t2htis=False):
    if edutus==None:
        alg_y=max(vahe_vasak+veerg*ruudupikkus-40-(len(edu_val)*ruudupikkus//2),vahe_vasak//2)
        X=vahe_peal+rida*ruudupikkus-10
        if X+80>aknake:
            X=vahe_peal+(rida-1)*ruudupikkus-70
        Edu_Nupp=Valik("Valik",edu_val,edu_val,alg_y,X,vahe=-1,Pilt=list(mabestik[ed] for ed in edu_val))
        #valiku_pinnad=[pygame.Rect(alg_y+v*80,vahe_peal+rida*ruudupikkus-10,80,80) for v in range(len(edu_val))]
        while edutus==None:

            for event in pygame.event.get():
                #Lahkub mängust, kui ESC või aken kinni nupp
            
                if event.type == QUIT:
                    return 3
                elif event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        return 2
                elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                    if Edu_Nupp.h6ljund:
                        edutus=Edu_Nupp.nupulist[Edu_Nupp.klikk()].nimi
                    #for pind in range(len(valiku_pinnad)):
                    #    if valiku_pinnad[pind].collidepoint(pygame.mouse.get_pos()):
                    #        edutus=edu_val[pind]
                    
                pygame.draw.rect(aken,tausta_v2rv,(alg_y,X,80*len(edu_val),80))
                Edu_Nupp.hiir()
                
                if False:
                    
                    pygame.draw.rect(aken,teksti_v2rv,(alg_y,X,80*len(edu_val),80),1)
                    for pind in range(len(valiku_pinnad)):
                        aken.blit(mabestik[edu_val[pind].lower() if pool==-1 else edu_val[pind]],(alg_y+pind*80,X))
                        if valiku_pinnad[pind].collidepoint(pygame.mouse.get_pos()):
                            pygame.draw.rect(aken,teksti_v2rv,(alg_y+pind*80,X,80,80),1)
                pygame.display.flip()
    #Kabenupp
    if edutus.upper()=="N":
        mab=K_mabend(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=t2htis)
    #Tamka
    elif edutus.upper()=="T":
        mab=Tamka(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=t2htis)
    #Ettur
    elif edutus.upper()=="E":
        mab=Ettur(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=t2htis)
    #Oda
    elif edutus.upper()=="O":
        mab=Oda(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=t2htis)
    #Vanker
    elif edutus.upper()=="V":
        mab=Vanker(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=t2htis)
    #Lipp
    elif edutus.upper()=="L":
        mab=Lipp(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=t2htis)
    #Ratsu
    elif edutus.upper()=="R":
        mab=Ratsu(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=t2htis)
    #Kuningas
    elif edutus.upper()=="K":
        mab=Kuningas(j2rg,pool,rida,veerg,vahe_vasak,vahe_peal,ruudupikkus,t2htis=t2htis)   
    else:
        mab="S"
    
    return(mab,edutus)

#Millised nupud saaksid olla ohtlikud v22risjupile
def Ohud(t2htsad,jupid,laud):
    oht=[]
    read=len(laud)-2
    veerud=len(laud[0])-2
    for mabend in jupid:
        #Etturit, ratsut ega kuningat pole m6tet vaadata, need niigi ei v6ta, kui pole juba tule all
        #Kas vanker on samal veerul v6i real
        if mabend.t2ht.upper()=="V":
            for t2htis in t2htsad:
                if t2htis[0]==mabend.rida or t2htis[1]==mabend.veerg:
                    oht.append(mabend.j2rg)          
        #Kas oda on samal diagonaalil
        elif mabend.t2ht.upper()=="O":
            for t2htis in t2htsad:
                if abs(t2htis[0]-mabend.rida)==abs(t2htis[1]-mabend.veerg):
                    oht.append(mabend.j2rg)
        #Kas lipp on samal veerul, real v6i diagonaalil
        elif mabend.t2ht.upper()=="L":
            for t2htis in t2htsad:
                if abs(t2htis[0]-mabend.rida)==abs(t2htis[1]-mabend.veerg) or t2htis[0]==mabend.rida or t2htis[1]==mabend.veerg:
                    oht.append(mabend.j2rg) 
        #Kas tamka/kabenupp on samal v2rvil ja kuningas pole seina 22res
        elif mabend.t2ht.upper() in ("N","T"):
            for t2htis in t2htsad:
                if (mabend.rida-mabend.veerg)%2==(t2htis[0]-t2htis[1])%2 and t2htis[0] not in (1,read) and t2htis[1] not in (1,veerud):
                    oht.append(mabend.j2rg)
    return(oht)

#Üks mäng mabet. Laud=mängunuppude algpositsioon
def Mäng(m2ngulaud):
    if m2ngulaud==None:
        m2ngulaud=uus_m2ng()
        if m2ngulaud==3:
            return 3
        if m2ngulaud==2:
            return 2
    m2ngulaud_tagavara=m2ngulaud[0].copy()
        
        
    laud=m2ngulaud[0]
    omadused=m2ngulaud[1]
    edutused=omadused["edutused"]
    read=len(laud)-2
    veerud=len(laud[0])-2

    ruudupikkus=int(laua_laius/max(read,veerud))
    
    #font=pygame.font.SysFont(Kirjastiil, fondisuurus)
    debug_font=pygame.font.SysFont(Kirjastiil, fondisuurus//2)
    
    #et oleks enam-vähem akna keskel mängulaud
    lisa_vahe_peal=(laua_laius-(read*ruudupikkus))/2
    lisa_vahe_vasak=(laua_laius-(veerud*ruudupikkus))/2
    vahe_peal=int(äär+lisa_vahe_peal)
    vahe_vasak=int(äär+lisa_vahe_vasak)
    #Astutavate nuppude v2rv
    v6tt_v2rv=tuple(v+50 for v in valitud_v2rv)
    #Mabendite kujutised
    pildid=mabendijoonised_s6nastikku(ruudupikkus)
    #Edutamise valiku pildid
    edu_pildid=mabendijoonised_s6nastikku(80)
    #Eri poolte mabendid
    mabend_pool=[0,[],[]]
    #Mabendid, mida ei tohi v6tta
    t2htsad=[0,[],[]]
    
    #Teeb mabendite objektid
    for R in range(len(laud)):
        for V in range(len(laud[R])):
            if laud[R][V] not in ("_","S"):
                #Kuna edutamine teeb vastavate omadustega nupu, siis sellega ka nuppude alguses tegemine
                mabend_pool[kumb_pool(laud[R][V])].append(edutamine("",edu_pildid,len(mabend_pool[kumb_pool(laud[R][V])]),kumb_pool(laud[R][V]),R,V,vahe_vasak,vahe_peal,ruudupikkus, edutus=laud[R][V], t2htis=True if laud[R][V] in omadused["t2htis"] or (R,V) in omadused["t2htis"] else False)[0])
                if laud[R][V] in omadused["t2htis"] or (R,V) in omadused["t2htis"]:
                    t2htsad[kumb_pool(laud[R][V])].append((R,V))  
    #Menyy nupud
    lahkumis_nupp=Nupp("Lahku","Lahku",0,0,fondisuurus=fondisuurus//2,raam=True)
    lahkumis_nupp.muudatus(y=laua_laius+äär*2-lahkumis_nupp.laius)
    nupud=[lahkumis_nupp]
    
    
    #V6imalikud k2igud s6jaudu, v6idu ja muu jaoks
    voim_k2igud=[0,[1],[1]]
    #K6ik tule all olevad ruudud kummagi poole jaoks
    suur_tuli=[0,[],[]]
    #T2htsatele ohtlikud mabendid
    t2ht_oht=[]
    #Sundv6tu jaoks
    Sund=False
    #Kas killstreak kabendil
    k2ia=None
    #Kui saab en passantiga, siis koordinaadid
    en_pas=None
    #Yhes kohas m2letamiseks
    temp_asi=False
    #V6itnud pool
    V=None
    #mabend_k2igud[kord] indeks
    Valitud=None
    #Kui True, siis valge, kui False, siis must
    #pool_t6lk=[-1,1]
    #Kelle kord
    kord=omadused["kord"]
    #Et ei peaks iga kord k6ike tegema
    Muut=True
    
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui ESC või aken kinni nupp
        
            if event.type == QUIT:
                salv=valik("Kas sa tahad enne lahkumist mängu salvestada?",["Jah","Ei","Tagasi"])
                #Küsib nime
                if salv=="Jah":
                    laud_faili("Viimatine",laud,omadused)
                    return 3
                #Lahkub
                if salv=="Ei":
                    return 3
                if salv==3:
                    return 3
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    salv=valik("Kas sa tahad enne lahkumist mängu salvestada?",["Jah","Ei","Tagasi"])
                    #Küsib nime
                    if salv=="Jah":
                        omadused["kord"]=kord
                        laud_faili("Viimatine",laud,omadused)
                        return 2
                    #Lahkub
                    if salv=="Ei":
                        return 2
                    if salv==3:
                        return 3
                #Käigu vahele jätmine
                if event.key == K_SPACE and (omadused["oota"] or (k2ia and not omadused["sund"])):
                    Valitud=None
                    kord*=-1
                    Muut=True
                    k2ia=False
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                pos=pygame.mouse.get_pos()
                #Kui klikk oli mänguvälja sees
                if vahe_peal<pos[1]<(read*ruudupikkus+vahe_peal) and vahe_vasak<pos[0]<(veerud*ruudupikkus+vahe_vasak):
                    rida=int((pos[1]-vahe_peal)//ruudupikkus)+1
                    veerg=int((pos[0]-vahe_vasak)//ruudupikkus)+1
                    #Kui on õige poole nupp
                    if kumb_pool(laud[rida][veerg]) == kord:
                        if Valitud!=None:
                            #Valitud pole enam valitud
                            mabend_pool[kord][Valitud].valitud=False
                        #Vaatab, millisele klikiti  
                        for M in mabend_pool[kord]:
                            M.klikk(laud)
                            if M.valitud:
                                Valitud=M.j2rg
                    else:
                        #Kui on midagi valitud
                        if Valitud!=None:
                            #Kui valitud mabend saab sinna k2ia, pole muul sundvõttu ja kas on käigu esimene võtt või varem võtnud(Kabendi killstreak) 
                            if  ((rida,veerg) in mabend_pool[kord][Valitud].k2igud and not Sund) or (rida,veerg) in mabend_pool[kord][Valitud].v6tud and (not k2ia or Valitud==k2ia):
                                #Kui mabend sooritas en passanti
                                if mabend_pool[kord][Valitud].t2ht.upper()=="E" and (rida,veerg)==en_pas:
                                    temp_asi=True
                                #Kui en passant on m2ngus
                                if omadused["en_passant"]:
                                    en_pas=False
                                    if mabend_pool[kord][Valitud].t2ht.upper()=="E":
                                        if not mabend_pool[kord][Valitud].liikund and abs(rida-mabend_pool[kord][Valitud].rida)==2:
                                            en_pas=(mabend_pool[kord][Valitud].rida+kord,mabend_pool[kord][Valitud].veerg)
                                    
                                #Vangerdamine
                                if mabend_pool[kord][Valitud].t2ht.upper()=="K" and omadused["vangerdus"] and abs(mabend_pool[kord][Valitud].veerg-veerg)>1:
                                    #Liigutab vankri
                                    suund=int((veerg-mabend_pool[kord][Valitud].veerg)/abs(veerg-mabend_pool[kord][Valitud].veerg))
                                    for jupend in mabend_pool[kord]:
                                        if jupend.rida==rida and jupend.veerg==veerg+suund:
                                            laud=jupend.k2ik(laud,(rida,veerg-suund))[0]
                                #Liigutab tähtsat nuppu
                                if (mabend_pool[kord][Valitud].rida,mabend_pool[kord][Valitud].veerg) in t2htsad[kord]:
                                    t2htsad[kord].remove((mabend_pool[kord][Valitud].rida,mabend_pool[kord][Valitud].veerg))
                                    t2htsad[kord].append((rida,veerg))

                                    
                                    
                                K2IK=mabend_pool[kord][Valitud].k2ik(laud,(rida,veerg))
                                laud=K2IK[0]
                                                            
                                #Kui en passant
                                if temp_asi:
                                    temp_asi=False
                                    K2IK[1]=(rida-kord,veerg)
                                    laud[rida-kord][veerg]="_"
                                
                                Muut=True
                                if K2IK[1]!=None:
                                    #Kustutab võetud mabendi
                                    for M in mabend_pool[-kord]:
                                        if M.kast.collidepoint(vahe_vasak+K2IK[1][1]*ruudupikkus-1,vahe_peal+K2IK[1][0]*ruudupikkus-1):
                                            #Kui t2htis
                                            if M.t2htis:
                                                t2htsad[-kord].remove((M.rida,M.veerg))
                                                #Kui m2ng v6idetud
                                                if omadused["t2ht_k2s"]=="v6i" or (omadused["t2ht_k2s"]=="ja"  and t2htsad[-kord]==[]):
                                                    V=kord 
                                            M.surnd()
                                    
                                    #Kas saab veel v6tta
                                    if mabend_pool[kord][Valitud].t2ht.upper() in ("N","T"):
                                        mabend_pool[kord][Valitud].uuri(laud)
                                        k2ia=Valitud
                                        if mabend_pool[kord][Valitud].v6tud==[]:
                                            mabend_pool[kord][Valitud].valitud=False
                                            Valitud=None
                                            k2ia=None
                                            kord*=-1
                                    else:
                                        mabend_pool[kord][Valitud].valitud=False
                                        Valitud=None
                                        k2ia=None
                                        kord*=-1       
                                else:
                                    mabend_pool[kord][Valitud].valitud=False
                                    Valitud=None
                                    k2ia=None
                                    kord*=-1
                            else:
                                mabend_pool[kord][Valitud].valitud=False
                                Valitud=None
        #Joonistab m2ngulaua
        mabelaud(laud,vahe_vasak,vahe_peal)
        #K6ik tule all olevad ruudud punased
        if False:
            for tuli in suur_tuli[-kord]:
                ruudujoonis(tuli[0]-1,tuli[1]-1,punane,ruudupikkus,vahe_vasak,vahe_peal)        
        #Debug menüü
        if debug:
            if Valitud!=None:
                Mabend=mabend_pool[kord][Valitud]
                v22rtused=[Mabend.k2igud,Mabend.v6tud,Mabend.tuli,Mabend.j2rg,Mabend.pool,Mabend.rida,Mabend.veerg,Mabend.t2ht,Mabend.t2htis]
                v22rt_nimed=["Käigud: ","Võtud: ","Tuli: ","Järg: ","Pool: ","Rida: ","Veerg: ","Täht: ","Tähtis: "]
                for v22rt in range(len(v22rtused)):
                    aken.blit(debug_font.render(v22rt_nimed[v22rt]+str(v22rtused[v22rt]), True, teksti_v2rv),(10,5+v22rt*30))
                    
            pos=pygame.mouse.get_pos()
            rida=int((pos[1]-vahe_peal)//ruudupikkus)+1
            veerg=int((pos[0]-vahe_vasak)//ruudupikkus)+1
            muu=[pos[0],pos[1],en_pas,Valitud,rida,veerg,ruudupikkus]
            muu_nim=["X: ","Y: ","En passant: ","Valitud: ","Rida: ", "Veerg: ","Ruudupikkus: "]
            for m in range(len(muu)):
                tekstik=debug_font.render(muu_nim[m]+str(muu[m]), True, teksti_v2rv)
                aken.blit(tekstik,(875-tekstik.get_width(),5+m*30))


            aken.blit(debug_font.render("Tähtsad: "+str(t2htsad), True, teksti_v2rv),(20,800))
            aken.blit(debug_font.render("Ohud: "+str(t2ht_oht), True, teksti_v2rv),(20,830))
                
        if Valitud!=None:
            #M2rgib k6ik v6imalikud k2igud
            if val_pilt=="Nurk":
                mabendid_lauale(laud,pildid,vahe_vasak,vahe_peal,t2htsad=t2htsad[1]+t2htsad[2])
            valitud_ruut(mabend_pool[kord][Valitud].rida-1,mabend_pool[kord][Valitud].veerg-1,valitud_v2rv,ruudupikkus,vahe_vasak,vahe_peal,3)
            if not k2ia or Valitud==k2ia:
                if Sund:
                    for k2ik in mabend_pool[kord][Valitud].v6tud:
                        valitud_ruut(k2ik[0]-1,k2ik[1]-1,v6tt_v2rv,ruudupikkus,vahe_vasak,vahe_peal,1)
                else:
                    for k2ik in mabend_pool[kord][Valitud].k2igud:
                        valitud_ruut(k2ik[0]-1,k2ik[1]-1,v6tt_v2rv,ruudupikkus,vahe_vasak,vahe_peal,1)
            if val_pilt=="Värv":
                mabendid_lauale(laud,pildid,vahe_vasak,vahe_peal,t2htsad=t2htsad[1]+t2htsad[2])
                        
        else:   
            mabendid_lauale(laud,pildid,vahe_vasak,vahe_peal,t2htsad=t2htsad[1]+t2htsad[2])
        #Joonistab j2lle 22red, kui vaja
        if val_pilt=="Värv":
            pygame.draw.line(aken,teksti_v2rv,(vahe_vasak,vahe_peal),(vahe_vasak+veerud*ruudupikkus,vahe_peal),3)
            pygame.draw.line(aken,teksti_v2rv,(vahe_vasak,vahe_peal),(vahe_vasak,vahe_peal+read*ruudupikkus),3)
            pygame.draw.line(aken,teksti_v2rv,(vahe_vasak+veerud*ruudupikkus,vahe_peal),(vahe_vasak+veerud*ruudupikkus,vahe_peal+read*ruudupikkus),3)
            pygame.draw.line(aken,teksti_v2rv,(vahe_vasak,vahe_peal+read*ruudupikkus),(vahe_vasak+veerud*ruudupikkus,vahe_peal+read*ruudupikkus),3)
        
        #Kumma poole k2ik
        pygame.draw.rect(aken, teksti_v2rv,(100,880,40,40),1)
        if kord==-1:
            pygame.draw.rect(aken, mabendi_v2rv_1,(100,880,40,40))      

        pygame.display.flip()
        
        #V6it
        if V!=None:
            v=Voit(V)
            if v==3:
                return 3
            if v=="Lahku":
                return 2
            if v=="Uus mäng":
                return Mäng((m2ngulaud_tagavara,omadused))
                
        #Asjad, mida kontrollitakse ainult iga liigutuse j2rel (Joonistamise j2rel, et edutamine ilus oleks)                    
        if Muut:
            suur_tuli=[0,[],[]]
            Sund=False
            voim_k2igud[kord]=[]
            
            #Tule all olevad asjad
            for Mabend in mabend_pool[-kord]:
                Mabend.uuri(laud)
                for tuli in Mabend.tuli:
                    if tuli not in suur_tuli[-kord]:
                        suur_tuli[-kord].append(tuli)
            #Vaatab, mis nupud saaksid teoreetiliselt t2htsat haavata
            t2ht_oht=Ohud(t2htsad[kord],mabend_pool[-kord],laud)
            #Edutamine
            for Mab in range(len(laud[1])):
                #Vaatab esimest ja viimast rida
                for rida in (1,read):
                    Mabend=laud[rida][Mab]
                    #Kui must mabend on teisele poole j6udnud
                    if ((Mabend==Mabend.lower() and rida==1 and kord==1) or (Mabend==Mabend.upper() and rida==read and kord==-1)) and Mabend!="_":
                        #Kui saab edutada
                        if edutused[Mabend]!=[]:
                            edukas=0
                            #Otsib 6ige mabendi
                            for M in mabend_pool[kumb_pool(Mabend)]:
                                if M.rida==rida and M.veerg==Mab:
                                    edukas=M.j2rg
                                    t2htisus=M.t2htis
                            #H2vitab mabendi
                            mabend_pool[kumb_pool(Mabend)][edukas].surnd()
                            #Kui mitu valikut
                            if len(edutused[Mabend])>1:
                                #M2rgib valikut tegeva nupu
                                valitud_ruut(rida-1,Mab-1,valitud_v2rv,ruudupikkus,vahe_vasak,vahe_peal,3)
                            a=edutamine(edutused[Mabend],edu_pildid,mabend_pool[kumb_pool(Mabend)][edukas].j2rg,kumb_pool(Mabend),rida,Mab,vahe_vasak,vahe_peal,ruudupikkus,edutus=edutused[Mabend.upper()][0] if len(edutused[Mabend.upper()])==1 else None,t2htis=t2htisus)
                            
                            if a[1]=="S":
                                mabend_pool[kumb_pool(Mabend)][edukas].surnd()
                            else:
                                mabend_pool[kumb_pool(Mabend)][edukas]=a[0]
                                mabend_pool[kumb_pool(Mabend)][edukas].uuri(laud)
                                for i in mabend_pool[kumb_pool(Mabend)][edukas].k2igud:
                                    if i not in suur_tuli[kumb_pool(Mabend)]:
                                        suur_tuli[kumb_pool(Mabend)].append(i)
                            laud[rida][Mab]="S" if a[1]=="S" else a[0].t2ht
                            
            
            for Mabend in mabend_pool[kord]:
                
                Mabend.uuri(laud)
                #En Passant
                if Mabend.t2ht.upper()=="E" and omadused["en_passant"] and en_pas:
                    if Mabend.rida+Mabend.pool==en_pas[0] and abs(en_pas[1]-Mabend.veerg)==1:
                        Mabend.v6tud.append(en_pas)
                        Mabend.k2igud.append(en_pas)
                #Vangerdamine
                if Mabend.t2ht.upper()=="K" and (Mabend.rida,Mabend.veerg) not in suur_tuli[-kord] and not Mabend.liikund and omadused["vangerdus"]:
                    
                    v1=None
                    vahet=False
                    for ruut in range(len(laud[Mabend.rida])):
                        if ruut==Mabend.veerg:
                            if v1!=None:
                                Mabend.k2igud.append((Mabend.rida,v1+1))
                            vahet=True
                            continue
                        if laud[Mabend.rida][ruut].upper()=="V" and kumb_pool(laud[Mabend.rida][ruut])==Mabend.pool:
                            #Otsib 6ige vankri ja vaatab, kas on liikund
                            for jupend in mabend_pool[kord]:
                                if jupend.rida==Mabend.rida and jupend.veerg==ruut and not jupend.liikund:
                                    if vahet:
                                        Mabend.k2igud.append((Mabend.rida,ruut-1))
                                        break
                                    else:
                                        v1=ruut
                                        continue
                            if v1==ruut:
                                continue
                        #Kui miski on ees v6i tuli
                            
                        if laud[Mabend.rida][ruut]!="_" or (Mabend.rida,ruut) in suur_tuli[-kord]:
                            
                            v1=None
                            if vahet:
                                break
                            
                #Vaatab, mis nupud saaks teoreetiliselt t2htsat haavata, kui yksi t2htis ei tohi surra v6i kui aint yks t2htis:
                if omadused["t2ht_k2s"]=="v6i" or (omadused["t2ht_k2s"]=="ja" and len(t2htsad[kord])==1):
                    #Vaatab,kuhu mabend saab liikuda t2htsaid ohustamata:
                    #Kui t2htis nupp
                    if (Mabend.rida,Mabend.veerg) in t2htsad[kord]:
                        for k2ik in Mabend.k2igud.copy():
                            #Ei saa käia, kui paneb tulle
                            if k2ik in suur_tuli[-kord]:
                                Mabend.k2igud.remove(k2ik)
                                if k2ik in Mabend.v6tud:
                                    Mabend.v6tud.remove(k2ik)
                                    continue
                            #List t2htsad, kus vaadatav mabend on liigutatud
                            hypot2htsad=t2htsad[kord].copy()
                            hypot2htsad.remove((Mabend.rida,Mabend.veerg))
                            hypot2htsad.append(k2ik)
                            hypo_oht=Ohud(hypot2htsad,mabend_pool[-kord],laud)
                            #Teeb laua, nagu oleks liikund sinna
                            uuslaud=[rida.copy() for rida in laud]
                            uuslaud[Mabend.rida][Mabend.veerg]="_"
                            uuslaud[k2ik[0]][k2ik[1]]=Mabend.t2ht
                            #Vaatab igat ohtlikku mabendit
                            for oht in hypo_oht:
                                M=mabend_pool[-kord][oht]
                                #kui pole mabendit v6etud
                                if uuslaud[M.rida][M.veerg]==M.t2ht:
                                    #Vaatab, kuhu mabend saaks k2ia ja taastab varasemad asjad.
                                    l_vana=[M.k2igud,M.v6tud,M.tuli]
                                    M.liik_uuendus(uuslaud)
                                    tuli=M.tuli
                                    M.k2igud=l_vana[0]
                                    M.v6tud=l_vana[1]
                                    M.tuli=l_vana[2]
                                    f=False
                                    #Kui 2hvardab t2htsat, siis ei saa k2ia
                                    for t2htis in hypot2htsad:
                                        if t2htis in tuli:
                                            if k2ik in Mabend.k2igud:
                                                Mabend.k2igud.remove(k2ik)
                                            if k2ik in Mabend.v6tud:
                                                Mabend.v6tud.remove(k2ik)
                                            f=True
                                            break
                                    if f:
                                        break
                    else:
                        for k2ik in Mabend.k2igud.copy():
                            #Teeb laua, nagu oleks liikund sinna
                            #uuslaud=[rida.copy() for rida in laud]
                            #uuslaud[Mabend.rida][Mabend.veerg]="_"
                            #uuslaud[k2ik[0]][k2ik[1]]=Mabend.t2ht
                            van=[Mabend.rida,Mabend.veerg]
                            uuslaud=Mabend.k2ik(laud,k2ik,t6sine=False)[0]
                            Mabend.rida=van[0]
                            Mabend.veerg=van[1]
                            #Vaatab igat ohtlikku mabendit
                            for oht in t2ht_oht:
                                M=mabend_pool[-kord][oht]
                                #kui pole mabendit v6etud
                                if uuslaud[M.rida][M.veerg]==M.t2ht:
                                    #Vaatab, kuhu mabend saaks k2ia ja taastab varasemad asjad.
                                    l_vana=[M.k2igud,M.v6tud,M.tuli]
                                    M.liik_uuendus(uuslaud)
                                    tuli=M.tuli.copy()
                                    M.k2igud=l_vana[0]
                                    M.v6tud=l_vana[1]
                                    M.tuli=l_vana[2]
                                    f=False
                                    
                                    #Kui 2hvardab t2htsat, siis ei saa k2ia
                                    for t2htis in t2htsad[kord]:
                                        if t2htis in tuli:
                                            Mabend.k2igud.remove(k2ik)
                                            if k2ik in Mabend.v6tud:
                                                Mabend.v6tud.remove(k2ik)
                                            f=True
                                            break
                                    if f:
                                        break
                                    
                #Tule all olevad asjad
                for tuli in Mabend.tuli:
                    if tuli not in suur_tuli[kord]:
                        suur_tuli[kord].append(tuli)
                
                for k2ik in Mabend.k2igud:
                    #Teeb listi v6imalikest k2ikudest
                    if k2ik not in voim_k2igud[kord]:
                        voim_k2igud[kord].append((Mabend.j2rg,k2ik))
                    #Kas sundv6tmine
                    Sund=True if k2ik in Mabend.v6tud and omadused["sund"] else Sund

            #Kui ei saa k2ia, siis vastaspool v6itnud(Siin, et j6aks enne m2ngulaua muuta)
            for k in (1,-1): 
                if voim_k2igud[k]==[]:
                    V=-k         
            Muut=False