import pygame
from pygame.locals import *

#Mustade ruutude värv
v2rv_1=(0, 0, 0)
#Valgete ruutude värv
v2rv_2=(255, 255, 255)
#Mustade mabendite ja valgete mabendite servade värv
mabendi_v2rv_1=(0, 0, 0)
#Valgete mabendite ja mustade mabendite servade värv
mabendi_v2rv_2=(255, 255, 255)
#Valitud nupu võimalike käikude värv
valitud_v2rv=(0, 200, 0)
#Seina värv
seina_v2rv=(100, 100, 100)
#Teksti ja raamide värv
teksti_v2rv=(0, 0, 0)
#Tausta värv
tausta_v2rv=(255, 255, 255)
#Punane värv kustutamise jaoks
punane=(200, 0, 0)
#Nt. kui ei saa käiku tagasi võtta
hall=(200, 200, 200)
#Tähtsad nupud
t2ht_v2rv=(218,165,32)

#Tähestik koordinaatide jaoks. Sellele võib lisada, kui tahad veel pikemat lauda
Tähestik=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T"]
#Fondisuurus
fondisuurus=30
#Mängulaua nime fondisuurus
nime_fondisuurus=60
#Kas salvestad tegevused logisse
logid=False
#Mis nuppudega saab käigu tagasi võtta
tagasi_klahvid=[K_z,K_RIGHT]
#Mis nuppudega saab käigu edasi liikuda
edasi_klahvid=[K_y,K_RIGHT]
#Nuppude piltide kaust(Pildid seal nimega, millega nad mabendilistis on)
mabendi_rada="Vaikimisi"
#Valitud ruudu graafika
val_pilt="Nurk"






#Mis redaktori logi numbriks on
R_logi_number=0
#Mis mängu logi numbriks on
M_logi_number=0

#Sätetest muutmatud sätted


#Vaikeomadused
vaike_omadused={"sund":False,"oota":True,"en_passant":True,"vangerdus":True,"t2ht_k2s":"ja","kord":1,"periood":0,"t2htis":["K","k"],"edutused":{"N":["T"],"E":["V","R","O","L"],"n":["t"],"e":["v","r","o","l"]}}
#Debug info
debug=False
#Sunnib akna väikseks
v2ike=True

#Numbrite klahvid
#numbri_klahvid=[K_0,K_1,K_2,K_3,K_4,K_5,K_6,K_7,K_8,K_9]
numbri_klahvid=["0","1","2","3","4","5","6","7","8","9"]
#List mabenditest, mis mängus on ("_" ja "S" peavad esimesed olema)
mabendid=["_","S","N","T","E","V","R","O","L","K"]
#Mängulaua maksimum-laius
laua_laius=640
#Vahe mängulaua ja akna ääre vahel
äär=120
#Värv, milleks piltide taustad pannakse, et musta tausta probleeme poleks
praak_v2rv=((204, 204, 0))
#Värv, milleks algul must muudetakse, et ei oleks  must=valge, valge=must  probleeme
vahe_v2rv=((0, 204, 204))
#Mängulauad, mida ei saa ise muuta(Male ja kabe on tavalised mängud, viimatine on fail pooleli jäänud mängu jaoks)
alg_lauad=["Male","Kabe","Viimatine","Nimetu Mängulaud"]
#Font
Kirjastiil="Arial"

