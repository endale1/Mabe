from mabe_funkt import *
from mabe_klassid import(Nupp,Lyliti,Tekstikast,Valik,Pikk_Valik)
         
#Teeb teate
def teade(tekst,aeg):
    font = pygame.font.SysFont(Kirjastiil, fondisuurus)
    kiri = font.render(tekst, True, teksti_v2rv)
    laius=(kiri.get_width()+20)
    vasak=(880-laius)//2
    pygame.draw.rect(aken,tausta_v2rv,(vasak,300,laius,kiri.get_height()+10))
    pygame.draw.rect(aken,teksti_v2rv,(vasak,300,laius,kiri.get_height()+10),1)
    aken.blit(kiri, (440-(kiri.get_width()//2), 305))
    pygame.display.flip()
    pygame.time.wait(aeg)
        
#Kas lahkuda mängust?
def valik(tekst,valikud):
    font = pygame.font.SysFont(Kirjastiil, fondisuurus)
    kyssa = font.render(tekst, True, teksti_v2rv)
    laius=max(200*len(valikud),(kyssa.get_width()+20))
    vasak=(880-laius)//2
    
    valiku_nupud=[]
    for i in range(len(valikud)):
        valiku_nupud.append(Nupp(valikud[i],valikud[i],vasak+200*(i+0.5),300,raam=False))

    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui ESC või aken kinni
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:                        
                    return 2
            
            elif event.type == QUIT:
                return 3

            elif event.type==pygame.MOUSEBUTTONDOWN:
                pos=pygame.mouse.get_pos()
                for n in valiku_nupud:
                    if n.h6ljund:
                        return(n.nimi)
        
        pygame.draw.rect(aken,tausta_v2rv,(vasak,160,laius,200))
        pygame.draw.rect(aken,teksti_v2rv,(vasak,160,laius,200),1)
        kyssa = font.render(tekst, True, teksti_v2rv)
        aken.blit(kyssa, (440-(kyssa.get_width()//2), 170))
        
        
        #Vaatab, kas mõne nupu peal hiir
        for n in valiku_nupud:
            n.hiir()

        
        pygame.display.flip()

#Joonistab kasti, kuhu saab kirjutada
def nimekast(kys,nimi="",maks_pikkus=100):
    font = pygame.font.SysFont(Kirjastiil, fondisuurus)
    kast=Tekstikast("Nimi",nimi,440,300,makspikkus=maks_pikkus)
    
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui ESC või aken kinni  
            if event.type == QUIT:
                return 2
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    #Et ei läheks muuga sassi
                    return(3)
                if event.key==K_RETURN:
                    return(kast.tekst)
                else:
                    kast.sisend(event)
                    kast.muudatus(y=450-kast.laius//2)
            elif event.type==pygame.MOUSEBUTTONDOWN:
                if kast.h6ljund:
                    kast.valitud=True
                    kast.just=True
                else:
                    kast.valitud=False
        #Et taust poleks ruuduline
        pygame.draw.rect(aken,tausta_v2rv,(200,240,520,200))
        pygame.draw.rect(aken,teksti_v2rv,(200,240,520,200),1)
        s = font.render(kys, True, teksti_v2rv)
        aken.blit(s, (340, 250))
        #Joonistab teksti
        kast.hiir()
        #Õige y
        

        pygame.display.flip()
        
        
#Avab mängu salvestamise akna(funktsioon, et saaks rekursiivne olla)
def salvesta(laud,omadused,nimi=""):
    nimi=nimekast("Kirjuta failinimi",nimi=nimi,maks_pikkus=20)
    #Kui vajutati Esc nuppu
    if nimi==2:
        return 2
    #Kui vajutati kinni panemise nuppu
    elif nimi==3:
        lahku=valik("Kas sa tahad enne lahkumist mängu salvestada?",["Jah","Ei","Tagasi"])
        #Kui Jah, tagasi või Esc-nupp
        if lahku in ("Jah","Tagasi",2) :
            return(4)
        #Lahkub
        if lahku=="Ei":
            #Et ei ajaks muuga sassi
            return(2)

    #Kui nimi pole tühi, on string ja pole muutmatu
    if nimi!="" and nimi==str(nimi) and nimi not in alg_lauad:
        laud_faili(nimi,laud,omadused)
    return(nimi)