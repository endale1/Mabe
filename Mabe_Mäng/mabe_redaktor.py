from graafika_funkt import *
from menüü_funkt import *
    

#Pole valmis
#Laseb valida laua pikkuse, laiuse ja muid sätteid
def editor_sätted():
    font=pygame.font.SysFont(Kirjastiil, fondisuurus)
    
    
    Valmis=Nupp("Valmis","Valmis",750,aknake-50)
    Tagasi=Nupp("Tagasi","Tagasi",600,aknake-50)
    nupud=[Valmis,Tagasi]
    
    laik=Tekstikast("Laius","8",350,620,tyhi="0",maks=str(len(Tähestik)),lubat=numbri_klahvid,fondisuurus=fondisuurus,minlaius=40)
    k6rk=Tekstikast("Kõrgus","8",350,720,tyhi="0",maks=str(len(Tähestik)),lubat=numbri_klahvid,fondisuurus=fondisuurus,minlaius=40)
    nimik=Tekstikast("Nimik","",550,820,tyhi="",fondisuurus=fondisuurus,minlaius=80)
    kastid=[laik,k6rk,nimik]
    
    kirjeldused=["Laua laius ruutudes","Laua kõrgus ruutudes","Failist võetud laud (Mitte kohustuslik)"]
    laud=lauatabel(8,8)
    suurus=60
    mabestik=(mabendijoonised_s6nastikku(suurus))
    Valitud=None
    
    #Kas on vajutatud enterit või klõpsatud kuskile, 
    ent=True

    kaardid=os.listdir("Kaardid")
    for kaart in range(len(kaardid)):
        #Võtab laiendi lõpu ära
        kaardid[kaart]=kaardid[kaart][:-4]
        
    #Kaardide listist valimine
    #Suur ekraan
    if Suur: 
        kaardi_nupud=Pikk_Valik("Kaart",kaardid,kaardid,30,900,800)
    else:
        kaardi_nupud=Pikk_Valik("Kaart",kaardid,kaardid,20,20,240,suund="vert",k6rgus=11)
    
    Flag=False
    #Kas m6ni on muutunud
    muut=[False,False,False]  
    
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui aken kinni nupp
            if event.type == QUIT:
                return 3
                
            #Klahvivajutused
            elif event.type == KEYDOWN:
                #Avab lahkumise men kui ESC
                if event.key == K_ESCAPE:
                    return 2
                if event.key in (K_RETURN,K_TAB):
                    if Valitud!=None:
                        kastid[Valitud].valitud=False
                        ent=True
                        #Kui shifti vajutatakse
                        if event.mod & pygame.KMOD_SHIFT:
                            #Võtab eelmise kasti lahti
                            if Valitud==0:
                                Valitud=None
                            else:
                                Valitud-=1
                                kastid[Valitud].valitud=True
                                kastid[Valitud].just=True
                        else:                            
                            #Võtab järgmise kasti lahti
                            if Valitud==2:
                                Valitud=None
                            else:
                                Valitud+=1
                                kastid[Valitud].valitud=True
                                kastid[Valitud].just=True
                elif Valitud!=None:
                    a=kastid[Valitud].tekst
                    kastid[Valitud].sisend(event)
                    if a!=kastid[Valitud].tekst:
                        muut[Valitud]=True
                    
            #Kui hiireklikk    
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                ent=True
                if Tagasi.h6ljund:
                    return 2
                if Valmis.h6ljund:
                    Flag=True
                    ent=True
                        
                
                #Kaartide loeng
                if kaardi_nupud.h6ljund:
                    kaardi_nupud.klikk()
                    #Kui kl6psati kaardile, mitte edasi-tagasi
                    if not kaardi_nupud.eel.h6ljund and not kaardi_nupud.j2r.h6ljund:
                        laud=laud_failist(kaardi_nupud.nupulist[kaardi_nupud.valitud].nimi)[0]
                        nimik.tekst=kaardi_nupud.nupulist[kaardi_nupud.valitud].nimi
                        laik.tekst=str(len(laud[0])-2)
                        k6rk.tekst=str(len(laud)-2)
                        
                #Kas kastile kl6psatud
                for k in range(len(kastid)):
                    if kastid[k].h6ljund:
                        Valitud=k
                        kastid[k].valitud=True
                        kastid[k].just=True
                    else:
                        kastid[k].valitud=False                  
                        
        #Lisab v6i kustutab veerge, kuni 6ige arv
        if muut[0] and ent:
            while len(laud[0])!=int(laik.tekst)+2:
                if len(laud[0])<int(laik.tekst)+2:
                    laud[0].insert(-1,"S")
                    for r in laud[1:-1]:
                        r.insert(-1,"_")
                    laud[-1].insert(-1,"S")
                else:
                    for r in laud:
                        r.pop(-1)
            muut[0]=False
            nimik.tekst=""
        #Lisab v6i kustutab ridu, kuni 6ige arv
        if muut[1] and ent:
            while len(laud)!=int(k6rk.tekst)+2:
                if len(laud)<(int(k6rk.tekst)+2):
                    laud.insert(-1,("S "+"_ "*(int(laik.tekst))+"S").split(" "))
                else:
                    laud.pop(-1)
            muut[1]=False
            nimik.tekst=""
            
        #Kui oli nimekastil ja on sobiv nimi, muudab laua, kui pole, siis tyhi laud.
        if muut[2] and nimik.tekst!="" and ent:
            muut[2]=False
            if nimik.tekst in kaardid:
                laud=laud_failist(nimik.tekst)[0]
                laik.tekst=str(len(laud[0])-2)
                k6rk.tekst=str(len(laud)-2)
            else:
                nimik.tekst=""
                laud=lauatabel(int(laik.tekst),int(k6rk.tekst))
        
        #Muudab vajadusel nuppude suuruse
        if suurus!=480//max(int(laik.tekst),int(k6rk.tekst)) and ent:
            suurus=480//max(int(laik.tekst),int(k6rk.tekst))
            mabestik=(mabendijoonised_s6nastikku(suurus))      
                
        
        omadused={"periood":0}
        sätted=[int(laik.tekst),int(k6rk.tekst),laud,omadused,nimik.tekst]  
        aken.fill(tausta_v2rv)
        
        #Näidislaud
        if ent:
            vahe_peal=60+(480-(int(k6rk.tekst)*suurus))/2
            vahe_vasak=(200 if Suur else 280+60)+(480-(int(laik.tekst)*suurus))/2
        
        mabelaud(laud,vahe_vasak,vahe_peal,suurus=480)
        mabendid_lauale(laud,mabestik,vahe_vasak,vahe_peal,suurus=480)
        #Jooned
        pygame.draw.line(aken,teksti_v2rv,(0,600),(880,600),3)
        #Lisajoon kui v2ike
        if v2ike:
            pygame.draw.line(aken,teksti_v2rv,(280,0),(280,600),3)
        #Kirjeldused:
        for kiri in range(len(kirjeldused)):
            aken.blit(font.render(kirjeldused[kiri],True,teksti_v2rv),(20,620+100*kiri))                 
        #Nupud ja tekstikastid
        for nupp in ([kaardi_nupud]+nupud+kastid):
            nupp.hiir()
        pygame.display.flip()
        ent=False
        if Flag:
            return(int(k6rk.tekst),int(laik.tekst),laud,omadused,nimik.tekst)    

 
#Laseb luua uue mängu algseisu
def mabe_redaktor():
    sätted=editor_sätted()
    if sätted in (2,3):
        return sätted
        
    
    read=sätted[0]
    veerud=sätted[1]
    laud=sätted[2]
    omadused=sätted[3]
    nimi=sätted[4]
    ruudupikkus=int(laua_laius/max(read,veerud))
    #Kui pikad on nupud mabendi valimiseks
    ikoonipikkus=min(int(800/len(mabendid)),80)
    
    nime_font = pygame.font.SysFont(Kirjastiil, nime_fondisuurus)
    
    #et oleks enam-vähem akna keskel mängulaud
    lisa_vahe_peal=(laua_laius-(read*ruudupikkus))/2
    lisa_vahe_vasak=(laua_laius-(veerud*ruudupikkus))/2
    vahe_peal=äär+lisa_vahe_peal
    vahe_vasak=äär+lisa_vahe_vasak
    
    Pool=1
    v2rv_t6lk=['0',mabendi_v2rv_2,mabendi_v2rv_1]
    Valitud="_"
    
    #Seab valmis menüü nupud
    salvestus_nupp=Nupp("Salvest","Salvesta",0,0,mink6rgus=25,fondisuurus=fondisuurus//2,raam=True)
    salvesta_nimega=Nupp("Nimega","Salvesta nimega",salvestus_nupp.laius,0,mink6rgus=25,fondisuurus=fondisuurus//2,raam=True)
    tagasi=Nupp("Tagasi","<-",salvesta_nimega.laius+salvesta_nimega.y,0,mink6rgus=25,fondisuurus=fondisuurus//2,raam=True)
    edasi=Nupp("Edasi","->",tagasi.y+tagasi.laius,0,mink6rgus=25,fondisuurus=fondisuurus//2,raam=True)
    lahkumis_nupp=Nupp("Lahku","Lahku",0,0,fondisuurus=fondisuurus//2,raam=True)
    lahkumis_nupp.muudatus(y=laua_laius+äär*2-lahkumis_nupp.laius)
    
    #Teeb pildid laual olevate mabendite ja ikoonide jaoks
    mabestik=(mabendijoonised_s6nastikku(ruudupikkus))
    ikoonid=(mabendijoonised_s6nastikku(80))
    #Seina pilt
    S=pygame.Surface((80,80))
    S.fill(hall)
    ikoonid["S"]=S
    #Tyhi ruut
    Tyhi=pygame.Surface((80,80))
    Tyhi.fill(tausta_v2rv)
    pygame.draw.line(Tyhi,punane,(0,0),(80,80))
    pygame.draw.line(Tyhi,punane,(0,80),(80,0))
    ikoonid["_"]=Tyhi
    Nupu_valik=Pikk_Valik("Valik",mabendid,mabendid,80 if aknake>800 else 20,140,80 if aknake<800 else 680,Pilt=list(ikoonid[Mab] for Mab in mabendid),valitud=0,suund="vert" if aknake<800 else "hor",k6rgus=5)
    
    #Teeb pildid Poole vahetamise nupu jaoks
    must=pygame.Surface((80,80))
    must.fill(mabendi_v2rv_1)
    pygame.draw.polygon(must,mabendi_v2rv_2,((0,0),(0,80),(80,80)))
    valge=pygame.Surface((80,80))
    valge.fill(mabendi_v2rv_2)
    pygame.draw.polygon(valge,mabendi_v2rv_1,((0,0),(0,80),(80,80)))
    Poole_pildid=[0,valge,must]
    Poole_nupp=Lyliti("Pool",20,aknake-100,valitud_raam=1,Pilt=Poole_pildid[Pool])
    
    nupud=[salvestus_nupp,salvesta_nimega,tagasi,edasi,lahkumis_nupp,Nupu_valik,Poole_nupp]
    
    #Et "Salvesta nimega" ei kustutaks nime
    t_nimi=nimi
    #Kas on peale viimatist salvestust muudetud
    Muut=False
    #Kas ollakse lahkumas
    lahk=False
    #Kas on lahti salvestamise ekraan
    s_ting=False  
    #Seab valmis logi
    k2ik=0
    logi=[]
    if logid:
        logi_fail=open(os.path.join(("Logid","R_logi#"+str(R_logi_number))),"w",encoding="UTF-8")
        uuenda_s2tted({"R_logi_number":R_logi_number+1})
    
    if aknake<800:
        vahe_vasak+=90
    while True:
        for event in pygame.event.get():
            #Lahkub mängust, kui aken kinni nupp
            if event.type == QUIT:
                if Muut:
                    salv=valik("Kas sa tahad enne lahkumist mängu salvestada?",["Jah","Ei","Tagasi"])
                    #Küsib nime
                    if salv=="Jah":
                        s_ting=True
                        lahk=2
                    #Lahkub
                    if salv=="Ei":
                        if logid:
                            logi_fail.write("Mäng kinni\n")
                            logi_fail.close()
                        return 3
                    if salv==3:
                        return 3
                else:
                    if logid:
                        logi_fail.write("Mäng kinni\n")
                        logi_fail.close()
                    return 3
            #Klahvivajutused
            elif event.type == KEYDOWN:
                #Avab lahkumise men kui ESC
                if event.key == K_ESCAPE:
                    lahku=valik("Mine tagasi avalehele?",["Jah","Ei"])
                    if lahku=="Jah":
                        if Muut:
                            salv=valik("Kas sa tahad enne lahkumist mängu salvestada?",["Jah","Ei","Tagasi"])
                            #Küsib nime
                            if salv=="Jah":
                                lahk=3
                                s_ting=True
                            #Lahkub
                            if salv=="Ei":
                                if logid:
                                    logi_fail.write("Avalehele\n")
                                    logi_fail.close()
                                return 2
                        else:
                            if logid:
                                logi_fail.write("Avalehele\n")
                                logi_fail.close()
                            return 2
                    if lahku==3:
                        return 3
                #Kui tahetakse tagasi võtta
                elif event.key in tagasi_klahvid and k2ik!=0:
                    laud=R_logi(laud,logi,k2ik,-1)
                    k2ik-=1
                    if logid:
                        logi_fail.write("Tagasi\n")
                #Kui tahetakse edasi minna
                elif event.key in edasi_klahvid and k2ik<len(logi):
                    laud=R_logi(laud,logi,k2ik,1)
                    k2ik+=1
                    if logid:
                        logi_fail.write("Edasi\n")
                #Kui vajutad mabendi algt'hega klahvile siis see valitakse
                elif event.unicode.upper() in mabendid:
                    Valitud=event.unicode.upper()
                    Nupu_valik.valitud=mabendid.index(Valitud)
                #Kui X, siis kustutamine
                elif event.unicode.upper()=="X":
                    Valitud="_"
                    Nupu_valik.valitud=mabendid.index(Valitud)
                #Kui tyhik, siis Poole muutus 
                elif event.unicode==" ":
                    Pool*=-1   
            #Kui hiireklikk    
            elif event.type==pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0]:
                pos=pygame.mouse.get_pos()
                #Kui klikk oli mänguvälja sees
                if vahe_peal<pos[1]<(read*ruudupikkus+vahe_peal) and vahe_vasak<pos[0]<(veerud*ruudupikkus+vahe_vasak):
                    rida=int((pos[1]-vahe_peal)//ruudupikkus+1)
                    veerg=int((pos[0]-vahe_vasak)//ruudupikkus+1)
                    Muutja=Valitud
                    #Kas must või valge?
                    if Pool==-1 and Valitud not in ("S","_"):
                        Muutja=Valitud.lower()
                    #Kui tegu muudab nuppu:
                    if Muutja!=laud[rida][veerg]:
                        if logid:
                            logi_fail.write(str(veerg)+","+str(rida)+":"+laud[rida][veerg]+"->"+Muutja+"\n") 
                        #Salvestab käigu logisse
                        if k2ik==len(logi):
                            logi.append([veerg,rida,laud[rida][veerg],Muutja])
                        #Kui on just käik tagasi võetud
                        else:
                            logi[k2ik]=[veerg,rida,laud[rida][veerg],Muutja]
                            #Et ei saaks võtta kolme võrra tagasi, teha käigu ja siis veel kaks käiku edasi minna
                            logi=logi[:k2ik+1]
                        k2ik+=1
                        laud[rida][veerg]=Muutja
                        Muut=True   
                #Kui klikk oli nuppude valiku peal    
                if Nupu_valik.h6ljund:
                    Nupu_valik.klikk()
                    Valitud=mabendid[Nupu_valik.valitud]
                    if logid:
                        logi_fail.write("Valitud:"+Valitud+"\n") 
                #Kui klikk oli Poole vahetuse nupul
                #elif äär+laua_laius+ikoonipikkus+50<pos[1]<äär+laua_laius+ikoonipikkus+130 and 40<pos[0]<120:
                if Poole_nupp.h6ljund:
                    Poole_nupp.klikk()
                    Pool*=-1
                    Poole_nupp.Pilt=Poole_pildid[Pool]
                    for nupp in Nupu_valik.nupulist[2:]:
                        nupp.Pilt=ikoonid[nupp.nimi if Pool==1 else nupp.nimi.lower()]
                    if logid:
                        logi_fail.write("Pool:"+str(Pool)+"\n")    
                else:    
                    if salvestus_nupp.h6ljund and salvestus_nupp.t66t:
                        s_ting=True
                    if salvesta_nimega.h6ljund:
                        s_ting=True
                        t_nimi=""
                    #Tagasi
                    if tagasi.h6ljund and k2ik!=0:
                        laud=R_logi(laud,logi,k2ik,-1)
                        k2ik-=1
                        if logid:
                            logi_fail.write("Tagasi\n")
                    #Edasi
                    if edasi.h6ljund and k2ik!=len(logi):
                        laud=R_logi(laud,logi,k2ik,1)
                        k2ik+=1
                        if logid:
                            logi_fail.write("Edasi\n")
                    if lahkumis_nupp.h6ljund:
                        if Muut:
                            salv=valik("Kas sa tahad enne lahkumist mängu salvestada?",["Jah","Ei","Tagasi"])
                            #Küsib nime
                            if salv=="Jah":
                                s_ting=True

                                lahk=2
                            #Lahkub
                            if salv=="Ei":
                                if logid:
                                    logi_fail.write("Mäng kinni\n")
                                    logi_fail.close()
                                return 2
                            if salv==3:
                                return 3
                        else:
                            
                            return 2
        aken.fill(tausta_v2rv)                
        mabelaud(laud,vahe_vasak,vahe_peal)
        mabendid_lauale(laud,mabestik,vahe_vasak,vahe_peal)
        if False:
            #Joonistab nuppude valimiseks menüü
            for mabend in range(len(mabendid)):
                if mabendid[mabend]=='_':
                    #X-kuju
                    pygame.draw.line(aken,punane,(mabend*ikoonipikkus+40+3,äär+laua_laius+40+3),((mabend+1)*ikoonipikkus+40-3,äär+laua_laius+40+ikoonipikkus-3),3)
                    pygame.draw.line(aken,punane,(mabend+1*ikoonipikkus+40-3,äär+laua_laius+40+3),((mabend)*ikoonipikkus+40+3,äär+laua_laius+40+ikoonipikkus-3),3)
                #Kui sein või valge
                elif mabendid[mabend]=='S' or Pool==1:
                    aken.blit(ikoonid[mabendid[mabend]],(ikoonipikkus*(mabend)+40,äär+laua_laius+40))
                #Kui must
                else:
                    aken.blit(ikoonid[mabendid[mabend].lower()],(ikoonipikkus*(mabend)+40,äär+laua_laius+40))
                    
            #Joonistab valitud nupu akna allotsas
            if Valitud=='_':
                pygame.draw.line(aken,punane,(((laua_laius-ikoonipikkus)//2+äär+3,äär+laua_laius+ikoonipikkus+50+3)),((laua_laius+ikoonipikkus)//2+äär-3,äär+laua_laius+ikoonipikkus+130-3),3)
                pygame.draw.line(aken,punane,(((laua_laius+ikoonipikkus)//2+äär-3,äär+laua_laius+ikoonipikkus+50+3)),((laua_laius-ikoonipikkus)//2+äär+3,äär+laua_laius+ikoonipikkus+130-3),3)
            elif Valitud=='S' or Pool==1:
                aken.blit(ikoonid[Valitud],(((laua_laius-ikoonipikkus)//2+äär),äär+laua_laius+ikoonipikkus+50))
            else:
                aken.blit(ikoonid[Valitud.lower()],(((laua_laius-ikoonipikkus)//2+äär),äär+laua_laius+ikoonipikkus+50))
            pygame.draw.rect(aken,teksti_v2rv,((laua_laius-ikoonipikkus)//2+äär,äär+laua_laius+ikoonipikkus+50, ikoonipikkus, ikoonipikkus),1)
        
            #Joonistab Poole vahetamise nupu
            pygame.draw.polygon(aken,v2rv_t6lk[Pool],((40,äär+laua_laius+ikoonipikkus+50),(120,äär+laua_laius+ikoonipikkus+50),(40,äär+laua_laius+ikoonipikkus+130)))
            pygame.draw.polygon(aken,v2rv_t6lk[-Pool],((120,970),(120,äär+laua_laius+ikoonipikkus+50),(40,äär+laua_laius+ikoonipikkus+130)))
            pygame.draw.rect(aken,v2rv_1,(40,äär+laua_laius+ikoonipikkus+50, 80, 80),1)
        
        #Joonistab valitud nupu
        aken.blit(Nupu_valik.nupulist[Nupu_valik.valitud].Pilt,(20,50))
        pygame.draw.rect(aken,teksti_v2rv,(20,50, 80, 80),1)
        #Nupud õiget värvi
        edasi.t66t=k2ik!=len(logi)
        tagasi.t66t=k2ik!=0
        salvestus_nupp.t66t=Muut
        #Joonistab menüü nupud
        for nupp in nupud:
            nupp.hiir()
        #Kirjutab mängulaua nime
        if nimi=="":
            Pealkiri=nime_font.render("Nimetu mängulaud", True, teksti_v2rv)
        else:
            Pealkiri=nime_font.render(nimi, True, teksti_v2rv)
        aken.blit(Pealkiri, (((laua_laius+äär*2)-Pealkiri.get_width())//2+(90 if aknake<800 else 0), 16)) 
        
        #Kui salvestamise aken peab lahti olema(See on nii, et lahkumise aken ei püsiks taustal)
        if s_ting:
            #Kui laual pole veel nime, küsib
            if t_nimi=="" or t_nimi in alg_lauad:
                s=salvesta(laud,omadused)
                #Kui nupust mäng kinni
                if s==3:
                    if logid:
                        logi_fail.write("Mäng kinni\n")
                        logi_fail.close()
                    return 3
                #Kui Esc-iga salvestamine kinni
                elif s==2:
                    lahk=False
                    s_ting=False
                    t_nimi=nimi
                    
                #Kui avati lahkumise menüü, aga pandi kinni
                elif s==4:
                    s_ting=True
                #Kui sai sobiva nime
                elif s in alg_lauad:
                    teade("Nimi on ühe algse mängulaua nimi",1000)
                    s_ting=False
                    lahk=False
                elif s!="":
                    teade("Salvestatud",1000)
                    if logid:
                        logi_fail.write("Salvestatud\n")
                    Muut=False
                    s_ting=False
                    nimi=s
                    t_nimi=nimi
                #Kui oli tühi nimi
                else:
                    lahk=False
                    s_ting=False
                    t_nimi=nimi
            
            #Kui on nimi                
            else:
                laud_faili(nimi,laud,omadused)
                
                teade("Salvestatud",1000)
                if logid:
                    logi_fail.write("Salvestatud\n")
                Muut=False
                s_ting=False
        
        #Kui tahetakse lahkuda ja on salvestatud
        if lahk==3 and not s_ting:
            if logid:
                logi_fail.write("Mäng kinni\n")
                logi_fail.close()
            return 3
        #Kui tahetakse avaekraanile naasta ja on salvestatud
        if lahk==2 and not s_ting:
            if logid:
                logi_fail.write("Avaekraanile\n")
                logi_fail.close()
            return 2
            
            
        
        pygame.display.flip()
