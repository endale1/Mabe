Mabe mängimiseks on vaja pygame moodulit.
Kui sa jooksutad Mabet thonnys, saab pygame moodulit, kui lähed
inglise keeles Tools->Manage Packages või
eesti keeles Tööriistad->Halda lisapakke,
otsid ülevalt pygame, valid selle, ja klikid install(i)

Seejärel tuleb avada 'avaleht.py' ja seda jooksutada. 
(Kas ülevalt rohelise nupu või F5 abil)

Kui oled mängu sees segaduses, on igasugu infot Teabe juures.